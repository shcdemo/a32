<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32416">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation for a generall fast</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32416 of text R34808 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing C3307). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A32416</idno>
    <idno type="STC">Wing C3307</idno>
    <idno type="STC">ESTC R34808</idno>
    <idno type="EEBO-CITATION">14817130</idno>
    <idno type="OCLC">ocm 14817130</idno>
    <idno type="VID">102738</idno>
    <idno type="PROQUESTGOID">2240899099</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A32416)</note>
    <note>Transcribed from: (Early English Books Online ; image set 102738)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1566:40)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation for a generall fast</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by John Bill, Christopher Barker, Thomas Newcomb, and Henry Hills ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1678.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at our court at Whitehall, the thirtieth day of March 1678. In the thirtieth year of our reign."</note>
      <note>Reproduction of original in the Huntington Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Proclamations -- Great Britain.</term>
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King. A proclamation for a generall fast.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1678</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>405</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-03</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-07</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-08</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2008-08</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32416-t">
  <body xml:id="A32416-e0">
   <div type="royal_proclamation" xml:id="A32416-e10">
    <pb facs="tcp:102738:1" rend="simple:additions" xml:id="A32416-001-a"/>
    <head xml:id="A32416-e20">
     <figure xml:id="A32416-e30">
      <p xml:id="A32416-e40">
       <w lemma="diev" pos="ffr" xml:id="A32416-001-a-0010">DIEV</w>
       <w lemma="et" pos="ffr" xml:id="A32416-001-a-0020">ET</w>
       <w lemma="mon" pos="ffr" xml:id="A32416-001-a-0030">MON</w>
       <w lemma="droit" pos="nn1" xml:id="A32416-001-a-0040">DROIT</w>
      </p>
      <p xml:id="A32416-e50">
       <w lemma="honi" pos="ffr" xml:id="A32416-001-a-0050">HONI</w>
       <w lemma="soit" pos="ffr" xml:id="A32416-001-a-0060">SOIT</w>
       <w lemma="qvi" pos="ffr" xml:id="A32416-001-a-0070">QVI</w>
       <w lemma="mal" pos="ffr" xml:id="A32416-001-a-0080">MAL</w>
       <w lemma="y" pos="ffr" xml:id="A32416-001-a-0090">Y</w>
       <w lemma="pense" pos="ffr" xml:id="A32416-001-a-0100">PENSE</w>
      </p>
      <figDesc xml:id="A32416-e60">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="A32416-e70">
     <w lemma="by" pos="acp" xml:id="A32416-001-a-0110">By</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-0120">the</w>
     <w lemma="King" pos="n1" xml:id="A32416-001-a-0130">King</w>
     <pc unit="sentence" xml:id="A32416-001-a-0131">.</pc>
     <pc unit="sentence" xml:id="A32416-001-a-0140"/>
    </head>
    <head type="sub" xml:id="A32416-e80">
     <w lemma="a" pos="d" xml:id="A32416-001-a-0150">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A32416-001-a-0160">PROCLAMATION</w>
     <w lemma="for" pos="acp" xml:id="A32416-001-a-0170">For</w>
     <w lemma="a" pos="d" xml:id="A32416-001-a-0180">a</w>
     <w lemma="general" pos="n1" reg="General" xml:id="A32416-001-a-0190">Generall</w>
     <w lemma="fast" pos="av-j" xml:id="A32416-001-a-0200">Fast</w>
     <pc unit="sentence" xml:id="A32416-001-a-0210">.</pc>
    </head>
    <opener xml:id="A32416-e90">
     <signed xml:id="A32416-e100">
      <w lemma="CHARLES" pos="nn1" xml:id="A32416-001-a-0220">CHARLES</w>
      <w lemma="r." pos="ab" xml:id="A32416-001-a-0230">R.</w>
      <pc unit="sentence" xml:id="A32416-001-a-0240"/>
     </signed>
    </opener>
    <p xml:id="A32416-e110">
     <w lemma="for" pos="acp" rend="decorinit" xml:id="A32416-001-a-0250">FOr</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-0260">the</w>
     <w lemma="implore" pos="vvg" xml:id="A32416-001-a-0270">imploring</w>
     <w lemma="a" pos="d" xml:id="A32416-001-a-0280">a</w>
     <w lemma="blessing" pos="n1" xml:id="A32416-001-a-0290">Blessing</w>
     <w lemma="from" pos="acp" xml:id="A32416-001-a-0300">from</w>
     <w lemma="almighty" pos="j" xml:id="A32416-001-a-0310">Almighty</w>
     <w lemma="God" pos="nn1" xml:id="A32416-001-a-0320">God</w>
     <w lemma="upon" pos="acp" xml:id="A32416-001-a-0330">upon</w>
     <w lemma="his" pos="po" xml:id="A32416-001-a-0340">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32416-001-a-0350">Majesty</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-0360">and</w>
     <w lemma="all" pos="d" xml:id="A32416-001-a-0370">all</w>
     <w lemma="his" pos="po" xml:id="A32416-001-a-0380">His</w>
     <w lemma="dominion" pos="n2" xml:id="A32416-001-a-0390">Dominions</w>
     <pc xml:id="A32416-001-a-0400">,</pc>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-0410">and</w>
     <w lemma="for" pos="acp" xml:id="A32416-001-a-0420">for</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-0430">the</w>
     <w lemma="avert" pos="n1-vg" xml:id="A32416-001-a-0440">averting</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-0450">of</w>
     <w lemma="those" pos="d" xml:id="A32416-001-a-0460">those</w>
     <w lemma="judgement" pos="n2" reg="judgements" xml:id="A32416-001-a-0470">Iudgments</w>
     <w lemma="which" pos="crq" xml:id="A32416-001-a-0480">which</w>
     <w lemma="our" pos="po" xml:id="A32416-001-a-0490">our</w>
     <w lemma="manifold" pos="j" xml:id="A32416-001-a-0500">manifold</w>
     <w lemma="sin" pos="n2" xml:id="A32416-001-a-0510">sins</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-0520">and</w>
     <w lemma="provocation" pos="n2" xml:id="A32416-001-a-0530">provocations</w>
     <w lemma="have" pos="vvb" xml:id="A32416-001-a-0540">have</w>
     <w lemma="most" pos="avs-d" xml:id="A32416-001-a-0550">most</w>
     <w lemma="just" pos="av-j" xml:id="A32416-001-a-0560">justly</w>
     <w lemma="deserve" pos="vvn" xml:id="A32416-001-a-0570">deserved</w>
     <pc xml:id="A32416-001-a-0580">;</pc>
     <w lemma="the" pos="d" xml:id="A32416-001-a-0590">The</w>
     <w lemma="king" pos="n2" xml:id="A32416-001-a-0600">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32416-001-a-0610">most</w>
     <w lemma="excellent" pos="j" xml:id="A32416-001-a-0620">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32416-001-a-0630">Majesty</w>
     <w lemma="have" pos="vvz" xml:id="A32416-001-a-0640">hath</w>
     <w lemma="think" pos="vvn" xml:id="A32416-001-a-0650">thought</w>
     <w lemma="fit" pos="j" xml:id="A32416-001-a-0660">fit</w>
     <pc xml:id="A32416-001-a-0670">,</pc>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-0680">and</w>
     <w lemma="do" pos="vvz" xml:id="A32416-001-a-0690">doth</w>
     <w lemma="hereby" pos="av" xml:id="A32416-001-a-0700">hereby</w>
     <w lemma="command" pos="n1" xml:id="A32416-001-a-0710">Command</w>
     <pc xml:id="A32416-001-a-0720">,</pc>
     <w lemma="that" pos="cs" xml:id="A32416-001-a-0730">That</w>
     <w lemma="a" pos="d" xml:id="A32416-001-a-0740">a</w>
     <w lemma="general" pos="n1" xml:id="A32416-001-a-0750">General</w>
     <w lemma="fast" pos="av-j" xml:id="A32416-001-a-0760">Fast</w>
     <pc xml:id="A32416-001-a-0770">,</pc>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-0780">and</w>
     <w lemma="day" pos="n1" xml:id="A32416-001-a-0790">day</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-0800">of</w>
     <w lemma="solemn" pos="j" xml:id="A32416-001-a-0810">solemn</w>
     <w lemma="humiliation" pos="n1" xml:id="A32416-001-a-0820">Humiliation</w>
     <w lemma="be" pos="vvi" xml:id="A32416-001-a-0830">be</w>
     <w lemma="keep" pos="vvn" xml:id="A32416-001-a-0840">kept</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-0850">and</w>
     <w lemma="observe" pos="vvn" xml:id="A32416-001-a-0860">observed</w>
     <w lemma="throughout" pos="acp" xml:id="A32416-001-a-0870">throughout</w>
     <w lemma="this" pos="d" xml:id="A32416-001-a-0880">this</w>
     <w lemma="his" pos="po" xml:id="A32416-001-a-0890">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32416-001-a-0900">Majesties</w>
     <w lemma="kingdom" pos="n1" xml:id="A32416-001-a-0910">Kingdom</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-0920">of</w>
     <hi xml:id="A32416-e120">
      <w lemma="England" pos="nn1" xml:id="A32416-001-a-0930">England</w>
      <pc xml:id="A32416-001-a-0940">,</pc>
     </hi>
     <w lemma="dominion" pos="n1" xml:id="A32416-001-a-0950">Dominion</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-0960">of</w>
     <hi xml:id="A32416-e130">
      <w lemma="Wales" pos="nn1" xml:id="A32416-001-a-0970">Wales</w>
      <pc xml:id="A32416-001-a-0980">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-0990">and</w>
     <w lemma="town" pos="n1" xml:id="A32416-001-a-1000">Town</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-1010">of</w>
     <hi xml:id="A32416-e140">
      <w lemma="Berwick" pos="nn1" xml:id="A32416-001-a-1020">Berwick</w>
     </hi>
     <w lemma="upon" pos="acp" xml:id="A32416-001-a-1030">upon</w>
     <hi xml:id="A32416-e150">
      <w lemma="Tweed" pos="nn1" xml:id="A32416-001-a-1040">Tweed</w>
      <pc unit="sentence" xml:id="A32416-001-a-1050">.</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-1060">And</w>
     <w lemma="to" pos="acp" xml:id="A32416-001-a-1070">to</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-1080">the</w>
     <w lemma="intent" pos="n1" xml:id="A32416-001-a-1090">intent</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-1100">the</w>
     <w lemma="same" pos="d" xml:id="A32416-001-a-1110">same</w>
     <w lemma="may" pos="vmb" xml:id="A32416-001-a-1120">may</w>
     <w lemma="be" pos="vvi" xml:id="A32416-001-a-1130">be</w>
     <w lemma="perform" pos="vvn" xml:id="A32416-001-a-1140">performed</w>
     <w lemma="with" pos="acp" xml:id="A32416-001-a-1150">with</w>
     <w lemma="all" pos="d" xml:id="A32416-001-a-1160">all</w>
     <w lemma="decency" pos="n1" xml:id="A32416-001-a-1170">Decency</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-1180">and</w>
     <w lemma="order" pos="n1" xml:id="A32416-001-a-1190">Order</w>
     <pc xml:id="A32416-001-a-1200">,</pc>
     <w lemma="his" pos="po" xml:id="A32416-001-a-1210">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32416-001-a-1220">Majesty</w>
     <w lemma="do" pos="vvz" xml:id="A32416-001-a-1230">doth</w>
     <w lemma="hereby" pos="av" xml:id="A32416-001-a-1240">hereby</w>
     <w lemma="publish" pos="vvb" xml:id="A32416-001-a-1250">Publish</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-1260">and</w>
     <w lemma="declare" pos="vvb" xml:id="A32416-001-a-1270">Declare</w>
     <w lemma="to" pos="acp" xml:id="A32416-001-a-1280">to</w>
     <w lemma="all" pos="d" xml:id="A32416-001-a-1290">all</w>
     <w lemma="his" pos="po" xml:id="A32416-001-a-1300">his</w>
     <w lemma="love" pos="j-vg" xml:id="A32416-001-a-1310">loving</w>
     <w lemma="subject" pos="n2" xml:id="A32416-001-a-1320">Subjects</w>
     <pc xml:id="A32416-001-a-1330">,</pc>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-1340">and</w>
     <w lemma="do" pos="vvz" xml:id="A32416-001-a-1350">doth</w>
     <w lemma="strict" pos="av-j" xml:id="A32416-001-a-1360">strictly</w>
     <w lemma="charge" pos="n1" xml:id="A32416-001-a-1370">Charge</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-1380">and</w>
     <w lemma="command" pos="n1" xml:id="A32416-001-a-1390">Command</w>
     <pc xml:id="A32416-001-a-1400">,</pc>
     <w lemma="that" pos="cs" xml:id="A32416-001-a-1410">That</w>
     <w lemma="on" pos="acp" xml:id="A32416-001-a-1420">on</w>
     <hi xml:id="A32416-e160">
      <w lemma="Wednesday" pos="nn1" xml:id="A32416-001-a-1430">Wednesday</w>
     </hi>
     <w lemma="be" pos="vvg" xml:id="A32416-001-a-1440">being</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-1450">the</w>
     <w lemma="ten" pos="ord" xml:id="A32416-001-a-1460">Tenth</w>
     <w lemma="day" pos="n1" xml:id="A32416-001-a-1470">day</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-1480">of</w>
     <hi xml:id="A32416-e170">
      <w lemma="April" pos="nn1" xml:id="A32416-001-a-1490">April</w>
     </hi>
     <w lemma="next" pos="ord" xml:id="A32416-001-a-1500">next</w>
     <pc xml:id="A32416-001-a-1510">,</pc>
     <w lemma="this" pos="d" xml:id="A32416-001-a-1520">this</w>
     <w lemma="fast" pos="j" xml:id="A32416-001-a-1530">Fast</w>
     <w lemma="shall" pos="vmb" xml:id="A32416-001-a-1540">shall</w>
     <w lemma="be" pos="vvi" xml:id="A32416-001-a-1550">be</w>
     <w lemma="religious" pos="av-j" xml:id="A32416-001-a-1560">religiously</w>
     <w lemma="observe" pos="vvn" xml:id="A32416-001-a-1570">observed</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-1580">and</w>
     <w lemma="keep" pos="vvn" xml:id="A32416-001-a-1590">kept</w>
     <w lemma="within" pos="acp" xml:id="A32416-001-a-1600">within</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-1610">the</w>
     <w lemma="city" pos="n2" xml:id="A32416-001-a-1620">Cities</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-1630">of</w>
     <hi xml:id="A32416-e180">
      <w lemma="London" pos="nn1" xml:id="A32416-001-a-1640">London</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-1650">and</w>
     <hi xml:id="A32416-e190">
      <w lemma="Westminster" pos="nn1" xml:id="A32416-001-a-1660">Westminster</w>
      <pc xml:id="A32416-001-a-1670">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-1680">and</w>
     <w lemma="other" pos="d" xml:id="A32416-001-a-1690">other</w>
     <w lemma="place" pos="n2" xml:id="A32416-001-a-1700">Places</w>
     <w lemma="within" pos="acp" xml:id="A32416-001-a-1710">within</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-1720">the</w>
     <w lemma="weekly" pos="j" xml:id="A32416-001-a-1730">weekly</w>
     <w lemma="bill" pos="n2" xml:id="A32416-001-a-1740">Bills</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-1750">of</w>
     <w lemma="mortality" pos="n1" xml:id="A32416-001-a-1760">Mortality</w>
     <pc xml:id="A32416-001-a-1770">;</pc>
     <w lemma="wherein" pos="crq" xml:id="A32416-001-a-1780">wherein</w>
     <w lemma="his" pos="po" xml:id="A32416-001-a-1790">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32416-001-a-1800">Majesty</w>
     <w lemma="in" pos="acp" xml:id="A32416-001-a-1810">in</w>
     <w lemma="his" pos="po" xml:id="A32416-001-a-1820">His</w>
     <w lemma="royal" pos="j" xml:id="A32416-001-a-1830">Royal</w>
     <w lemma="person" pos="n1" xml:id="A32416-001-a-1840">Person</w>
     <pc xml:id="A32416-001-a-1850">,</pc>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-1860">and</w>
     <w lemma="with" pos="acp" xml:id="A32416-001-a-1870">with</w>
     <w lemma="his" pos="po" xml:id="A32416-001-a-1880">His</w>
     <w lemma="royal" pos="j" xml:id="A32416-001-a-1890">Royal</w>
     <w lemma="family" pos="n1" xml:id="A32416-001-a-1900">Family</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-1910">and</w>
     <w lemma="household" pos="n1" reg="Household" xml:id="A32416-001-a-1920">Houshold</w>
     <w lemma="will" pos="vmb" xml:id="A32416-001-a-1930">will</w>
     <w lemma="give" pos="vvi" xml:id="A32416-001-a-1940">give</w>
     <w lemma="example" pos="n1" xml:id="A32416-001-a-1950">Example</w>
     <w lemma="to" pos="acp" xml:id="A32416-001-a-1960">to</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-1970">the</w>
     <w lemma="rest" pos="n1" xml:id="A32416-001-a-1980">rest</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-1990">of</w>
     <w lemma="his" pos="po" xml:id="A32416-001-a-2000">His</w>
     <w lemma="people" pos="n1" xml:id="A32416-001-a-2010">People</w>
     <pc xml:id="A32416-001-a-2020">;</pc>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-2030">and</w>
     <w lemma="that" pos="cs" xml:id="A32416-001-a-2040">that</w>
     <w lemma="on" pos="acp" xml:id="A32416-001-a-2050">on</w>
     <hi xml:id="A32416-e200">
      <w lemma="Wednesday" pos="nn1" xml:id="A32416-001-a-2060">Wednesday</w>
     </hi>
     <w lemma="the" pos="d" xml:id="A32416-001-a-2070">the</w>
     <w lemma="four" pos="crd" xml:id="A32416-001-a-2080">Four</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-2090">and</w>
     <w lemma="twenty" pos="ord" xml:id="A32416-001-a-2100">twentieth</w>
     <w lemma="day" pos="n1" xml:id="A32416-001-a-2110">day</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-2120">of</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-2130">the</w>
     <w lemma="say" pos="j-vn" xml:id="A32416-001-a-2140">said</w>
     <w lemma="month" pos="n1" xml:id="A32416-001-a-2150">Month</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-2160">of</w>
     <hi xml:id="A32416-e210">
      <w lemma="April" pos="nn1" xml:id="A32416-001-a-2170">April</w>
      <pc xml:id="A32416-001-a-2180">,</pc>
     </hi>
     <w lemma="the" pos="d" xml:id="A32416-001-a-2190">the</w>
     <w lemma="like" pos="j" xml:id="A32416-001-a-2200">like</w>
     <w lemma="be" pos="vvi" xml:id="A32416-001-a-2210">be</w>
     <w lemma="observe" pos="vvn" xml:id="A32416-001-a-2220">observed</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-2230">and</w>
     <w lemma="keep" pos="vvn" xml:id="A32416-001-a-2240">kept</w>
     <w lemma="throughout" pos="acp" xml:id="A32416-001-a-2250">throughout</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-2260">the</w>
     <w lemma="rest" pos="n1" xml:id="A32416-001-a-2270">rest</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-2280">of</w>
     <w lemma="this" pos="d" xml:id="A32416-001-a-2290">this</w>
     <w lemma="his" pos="po" xml:id="A32416-001-a-2300">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32416-001-a-2310">Majesties</w>
     <w lemma="kingdom" pos="n1" xml:id="A32416-001-a-2320">Kingdom</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-2330">of</w>
     <hi xml:id="A32416-e220">
      <w lemma="England" pos="nn1" xml:id="A32416-001-a-2340">England</w>
      <pc xml:id="A32416-001-a-2350">,</pc>
     </hi>
     <w lemma="the" pos="d" xml:id="A32416-001-a-2360">the</w>
     <w lemma="dominion" pos="n1" xml:id="A32416-001-a-2370">Dominion</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-2380">of</w>
     <hi xml:id="A32416-e230">
      <w lemma="Wales" pos="nn1" xml:id="A32416-001-a-2390">Wales</w>
      <pc xml:id="A32416-001-a-2400">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-2410">and</w>
     <w lemma="town" pos="n1" xml:id="A32416-001-a-2420">Town</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-2430">of</w>
     <hi xml:id="A32416-e240">
      <w lemma="Berwick" pos="nn1" xml:id="A32416-001-a-2440">Berwick</w>
     </hi>
     <w lemma="upon" pos="acp" xml:id="A32416-001-a-2450">upon</w>
     <hi xml:id="A32416-e250">
      <w lemma="Tweed" pos="nn1" xml:id="A32416-001-a-2460">Tweed</w>
      <pc unit="sentence" xml:id="A32416-001-a-2470">.</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-2480">And</w>
     <w lemma="his" pos="po" xml:id="A32416-001-a-2490">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32416-001-a-2500">Majesty</w>
     <w lemma="do" pos="vvz" xml:id="A32416-001-a-2510">doth</w>
     <w lemma="most" pos="avs-d" xml:id="A32416-001-a-2520">most</w>
     <w lemma="strict" pos="av-j" xml:id="A32416-001-a-2530">strictly</w>
     <w lemma="charge" pos="vvi" xml:id="A32416-001-a-2540">Charge</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-2550">and</w>
     <w lemma="command" pos="vvi" xml:id="A32416-001-a-2560">Command</w>
     <w lemma="all" pos="d" xml:id="A32416-001-a-2570">all</w>
     <w lemma="his" pos="po" xml:id="A32416-001-a-2580">His</w>
     <w lemma="love" pos="j-vg" xml:id="A32416-001-a-2590">loving</w>
     <w lemma="subject" pos="n2" xml:id="A32416-001-a-2600">Subjects</w>
     <pc xml:id="A32416-001-a-2610">,</pc>
     <w lemma="that" pos="cs" xml:id="A32416-001-a-2620">That</w>
     <w lemma="they" pos="pns" xml:id="A32416-001-a-2630">they</w>
     <w lemma="do" pos="vvb" xml:id="A32416-001-a-2640">do</w>
     <w lemma="with" pos="acp" xml:id="A32416-001-a-2650">with</w>
     <w lemma="all" pos="d" xml:id="A32416-001-a-2660">all</w>
     <w lemma="christian" pos="jnn" xml:id="A32416-001-a-2670">Christian</w>
     <w lemma="reverence" pos="n1" xml:id="A32416-001-a-2680">Reverence</w>
     <w lemma="observe" pos="vvi" xml:id="A32416-001-a-2690">observe</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-2700">and</w>
     <w lemma="perform" pos="vvi" xml:id="A32416-001-a-2710">perform</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-2720">the</w>
     <w lemma="same" pos="d" xml:id="A32416-001-a-2730">same</w>
     <pc xml:id="A32416-001-a-2740">,</pc>
     <w lemma="according" pos="j" xml:id="A32416-001-a-2750">according</w>
     <w lemma="to" pos="acp" xml:id="A32416-001-a-2760">to</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-2770">the</w>
     <w lemma="direction" pos="n2" xml:id="A32416-001-a-2780">Directions</w>
     <w lemma="that" pos="cs" xml:id="A32416-001-a-2790">that</w>
     <w lemma="shall" pos="vmb" xml:id="A32416-001-a-2800">shall</w>
     <w lemma="be" pos="vvi" xml:id="A32416-001-a-2810">be</w>
     <w lemma="give" pos="vvn" xml:id="A32416-001-a-2820">given</w>
     <w lemma="in" pos="acp" xml:id="A32416-001-a-2830">in</w>
     <w lemma="a" pos="d" xml:id="A32416-001-a-2840">a</w>
     <w lemma="form" pos="n1" xml:id="A32416-001-a-2850">Form</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-2860">of</w>
     <w lemma="prayer" pos="n1" xml:id="A32416-001-a-2870">Prayer</w>
     <w lemma="to" pos="prt" xml:id="A32416-001-a-2880">to</w>
     <w lemma="be" pos="vvi" xml:id="A32416-001-a-2890">be</w>
     <w lemma="by" pos="acp" xml:id="A32416-001-a-2900">by</w>
     <w lemma="our" pos="po" xml:id="A32416-001-a-2910">Our</w>
     <w lemma="order" pos="n1" xml:id="A32416-001-a-2920">Order</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-2930">and</w>
     <w lemma="command" pos="vvb" xml:id="A32416-001-a-2940">Command</w>
     <w lemma="publish" pos="vvn" xml:id="A32416-001-a-2950">Published</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-2960">and</w>
     <w lemma="disperse" pos="vvn" xml:id="A32416-001-a-2970">dispersed</w>
     <w lemma="for" pos="acp" xml:id="A32416-001-a-2980">for</w>
     <w lemma="that" pos="d" xml:id="A32416-001-a-2990">that</w>
     <w lemma="purpose" pos="n1" xml:id="A32416-001-a-3000">purpose</w>
     <pc xml:id="A32416-001-a-3010">,</pc>
     <w lemma="as" pos="acp" xml:id="A32416-001-a-3020">as</w>
     <w lemma="they" pos="pns" xml:id="A32416-001-a-3030">they</w>
     <w lemma="tender" pos="vvb" xml:id="A32416-001-a-3040">tender</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-3050">the</w>
     <w lemma="honour" pos="n1" xml:id="A32416-001-a-3060">Honour</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-3070">of</w>
     <w lemma="almighty" pos="zz" xml:id="A32416-001-a-3080">Almighty</w>
     <w lemma="God" pos="nn1" xml:id="A32416-001-a-3090">God</w>
     <pc xml:id="A32416-001-a-3100">,</pc>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-3110">and</w>
     <w lemma="will" pos="vmd" xml:id="A32416-001-a-3120">would</w>
     <w lemma="avoid" pos="vvi" xml:id="A32416-001-a-3130">avoid</w>
     <w lemma="his" pos="po" xml:id="A32416-001-a-3140">his</w>
     <w lemma="just" pos="j" xml:id="A32416-001-a-3150">just</w>
     <w lemma="wrath" pos="n1" xml:id="A32416-001-a-3160">Wrath</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-3170">and</w>
     <w lemma="indignation" pos="n1" xml:id="A32416-001-a-3180">Indignation</w>
     <w lemma="against" pos="acp" xml:id="A32416-001-a-3190">against</w>
     <w lemma="this" pos="d" xml:id="A32416-001-a-3200">this</w>
     <w lemma="land" pos="n1" xml:id="A32416-001-a-3210">Land</w>
     <pc xml:id="A32416-001-a-3220">,</pc>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-3230">and</w>
     <w lemma="upon" pos="acp" xml:id="A32416-001-a-3240">upon</w>
     <w lemma="pain" pos="n1" xml:id="A32416-001-a-3250">Pain</w>
     <w lemma="of" pos="acp" xml:id="A32416-001-a-3260">of</w>
     <w lemma="receive" pos="vvg" xml:id="A32416-001-a-3270">receiving</w>
     <w lemma="such" pos="d" xml:id="A32416-001-a-3280">such</w>
     <w lemma="punishment" pos="n1" xml:id="A32416-001-a-3290">punishment</w>
     <w lemma="as" pos="acp" xml:id="A32416-001-a-3300">as</w>
     <w lemma="his" pos="po" xml:id="A32416-001-a-3310">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32416-001-a-3320">Majesty</w>
     <w lemma="may" pos="vmb" xml:id="A32416-001-a-3330">may</w>
     <w lemma="just" pos="av-j" xml:id="A32416-001-a-3340">justly</w>
     <w lemma="inflict" pos="vvi" xml:id="A32416-001-a-3350">inflict</w>
     <w lemma="upon" pos="acp" xml:id="A32416-001-a-3360">upon</w>
     <w lemma="such" pos="d" xml:id="A32416-001-a-3370">such</w>
     <w lemma="as" pos="acp" xml:id="A32416-001-a-3380">as</w>
     <w lemma="shall" pos="vmb" xml:id="A32416-001-a-3390">shall</w>
     <w lemma="contemn" pos="vvi" xml:id="A32416-001-a-3400">contemn</w>
     <w lemma="or" pos="cc" xml:id="A32416-001-a-3410">or</w>
     <w lemma="neglect" pos="vvi" xml:id="A32416-001-a-3420">neglect</w>
     <w lemma="so" pos="av" xml:id="A32416-001-a-3430">so</w>
     <w lemma="religious" pos="j" xml:id="A32416-001-a-3440">Religious</w>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-3450">and</w>
     <w lemma="necessary" pos="j" xml:id="A32416-001-a-3460">necessary</w>
     <w lemma="a" pos="d" xml:id="A32416-001-a-3470">a</w>
     <w lemma="duty" pos="n1" xml:id="A32416-001-a-3480">Duty</w>
     <pc unit="sentence" xml:id="A32416-001-a-3490">.</pc>
    </p>
    <closer xml:id="A32416-e260">
     <dateline xml:id="A32416-e270">
      <w lemma="give" pos="vvn" xml:id="A32416-001-a-3500">Given</w>
      <w lemma="at" pos="acp" xml:id="A32416-001-a-3510">at</w>
      <w lemma="our" pos="po" xml:id="A32416-001-a-3520">Our</w>
      <w lemma="court" pos="n1" xml:id="A32416-001-a-3530">Court</w>
      <w lemma="at" pos="acp" xml:id="A32416-001-a-3540">at</w>
      <hi xml:id="A32416-e280">
       <w lemma="Whitehall" pos="nn1" xml:id="A32416-001-a-3550">Whitehall</w>
       <pc xml:id="A32416-001-a-3560">,</pc>
      </hi>
      <date xml:id="A32416-e290">
       <w lemma="the" pos="d" xml:id="A32416-001-a-3570">the</w>
       <w lemma="thirty" pos="ord" xml:id="A32416-001-a-3580">Thirtieth</w>
       <w lemma="day" pos="n1" xml:id="A32416-001-a-3590">day</w>
       <w lemma="of" pos="acp" xml:id="A32416-001-a-3600">of</w>
       <hi xml:id="A32416-e300">
        <w lemma="march" pos="n1" xml:id="A32416-001-a-3610">March</w>
       </hi>
       <w lemma="1678." pos="crd" xml:id="A32416-001-a-3620">1678.</w>
       <pc unit="sentence" xml:id="A32416-001-a-3630"/>
       <w lemma="in" pos="acp" xml:id="A32416-001-a-3640">In</w>
       <w lemma="the" pos="d" xml:id="A32416-001-a-3650">the</w>
       <w lemma="thirty" pos="ord" xml:id="A32416-001-a-3660">Thirtieth</w>
       <w lemma="year" pos="n1" xml:id="A32416-001-a-3670">year</w>
       <w lemma="of" pos="acp" xml:id="A32416-001-a-3680">of</w>
       <w lemma="our" pos="po" xml:id="A32416-001-a-3690">Our</w>
       <w lemma="reign" pos="n1" xml:id="A32416-001-a-3700">Reign</w>
       <pc unit="sentence" xml:id="A32416-001-a-3710">.</pc>
      </date>
     </dateline>
     <lb xml:id="A32416-e310"/>
     <w lemma="God" pos="nn1" xml:id="A32416-001-a-3720">God</w>
     <w lemma="save" pos="vvb" xml:id="A32416-001-a-3730">save</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-3740">the</w>
     <w lemma="King" pos="n1" xml:id="A32416-001-a-3750">King</w>
     <pc unit="sentence" xml:id="A32416-001-a-3751">.</pc>
     <pc unit="sentence" xml:id="A32416-001-a-3760"/>
    </closer>
   </div>
  </body>
  <back xml:id="A32416-e320">
   <div type="colophon" xml:id="A32416-e330">
    <p xml:id="A32416-e340">
     <hi xml:id="A32416-e350">
      <w lemma="LONDON" pos="nn1" xml:id="A32416-001-a-3770">LONDON</w>
      <pc xml:id="A32416-001-a-3780">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A32416-001-a-3790">Printed</w>
     <w lemma="by" pos="acp" xml:id="A32416-001-a-3800">by</w>
     <hi xml:id="A32416-e360">
      <w lemma="John" pos="nn1" xml:id="A32416-001-a-3810">John</w>
      <w lemma="bill" pos="n1" xml:id="A32416-001-a-3820">Bill</w>
      <pc xml:id="A32416-001-a-3830">,</pc>
      <w lemma="Christopher" pos="nn1" xml:id="A32416-001-a-3840">Christopher</w>
      <w lemma="Barker" pos="nn1" xml:id="A32416-001-a-3850">Barker</w>
      <pc xml:id="A32416-001-a-3860">,</pc>
      <w lemma="Thomas" pos="nn1" xml:id="A32416-001-a-3870">Thomas</w>
      <w lemma="newcomb" pos="nn1" xml:id="A32416-001-a-3880">Newcomb</w>
      <pc xml:id="A32416-001-a-3890">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32416-001-a-3900">and</w>
     <hi xml:id="A32416-e370">
      <w lemma="Henry" pos="nn1" xml:id="A32416-001-a-3910">Henry</w>
      <w lemma="hill" pos="n2" xml:id="A32416-001-a-3920">Hills</w>
      <pc xml:id="A32416-001-a-3930">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A32416-001-a-3940">Printers</w>
     <w lemma="to" pos="acp" xml:id="A32416-001-a-3950">to</w>
     <w lemma="the" pos="d" xml:id="A32416-001-a-3960">the</w>
     <w lemma="king" pos="n2" xml:id="A32416-001-a-3970">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32416-001-a-3980">most</w>
     <w lemma="excellent" pos="j" xml:id="A32416-001-a-3990">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32416-001-a-4000">Majesty</w>
     <pc unit="sentence" xml:id="A32416-001-a-4010">.</pc>
     <w lemma="1678." pos="crd" xml:id="A32416-001-a-4020">1678.</w>
     <pc unit="sentence" xml:id="A32416-001-a-4030"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
