<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32590">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation for suppression of popery</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32590 of text R35884 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing C3515A). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A32590</idno>
    <idno type="STC">Wing C3515A</idno>
    <idno type="STC">ESTC R35884</idno>
    <idno type="EEBO-CITATION">14867323</idno>
    <idno type="OCLC">ocm 14867323</idno>
    <idno type="VID">102746</idno>
    <idno type="PROQUESTGOID">2264226461</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A32590)</note>
    <note>Transcribed from: (Early English Books Online ; image set 102746)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1566:48 or 1588:86)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation for suppression of popery</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by the assigns of John Bill and Christopher Barker ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1673.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at our court at Whitehall the twentieth day of November, 1673. In the twenty fifth year of our reign."</note>
      <note>Ordering enforcement of the Penal laws against recusants.</note>
      <note>Item at reel 1566:48 identified as Wing C3515A (number cancelled).</note>
      <note>Reproduction of originals in the Harvard University Library and the Huntington Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Dissenters, Religious -- Legal status, laws, etc. -- England.</term>
     <term>Catholics -- England.</term>
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King. A proclamation for suppression of popery.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1673</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>385</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-07</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-10</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-11</date><label>John Pas</label>
        Sampled and proofread
      </change>
   <change><date>2008-11</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32590-t">
  <body xml:id="A32590-e0">
   <div type="proclamation" xml:id="A32590-e10">
    <pb facs="tcp:102746:1" xml:id="A32590-001-a"/>
    <byline xml:id="A32590-e20">
     <w lemma="by" pos="acp" xml:id="A32590-001-a-0010">By</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-0020">the</w>
     <w lemma="King" pos="n1" xml:id="A32590-001-a-0030">King</w>
     <pc unit="sentence" xml:id="A32590-001-a-0031">.</pc>
     <pc unit="sentence" xml:id="A32590-001-a-0040"/>
    </byline>
    <head xml:id="A32590-e30">
     <w lemma="a" pos="d" xml:id="A32590-001-a-0050">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A32590-001-a-0060">PROCLAMATION</w>
     <w lemma="for" pos="acp" xml:id="A32590-001-a-0070">For</w>
     <w lemma="suppression" pos="n1" xml:id="A32590-001-a-0080">Suppression</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-0090">of</w>
     <w lemma="popery" pos="n1" xml:id="A32590-001-a-0100">POPERY</w>
     <pc unit="sentence" xml:id="A32590-001-a-0110">.</pc>
    </head>
    <opener xml:id="A32590-e40">
     <signed xml:id="A32590-e50">
      <w lemma="CHARLES" pos="nn1" xml:id="A32590-001-a-0120">CHARLES</w>
      <w lemma="r." pos="ab" xml:id="A32590-001-a-0130">R.</w>
      <pc unit="sentence" xml:id="A32590-001-a-0140"/>
     </signed>
    </opener>
    <p xml:id="A32590-e60">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A32590-001-a-0150">WHereas</w>
     <w lemma="in" pos="acp" xml:id="A32590-001-a-0160">in</w>
     <w lemma="pursuance" pos="n1" xml:id="A32590-001-a-0170">pursuance</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-0180">of</w>
     <w lemma="our" pos="po" xml:id="A32590-001-a-0190">Our</w>
     <w lemma="gracious" pos="j" xml:id="A32590-001-a-0200">Gracious</w>
     <w lemma="assurance" pos="n2" xml:id="A32590-001-a-0210">Assurances</w>
     <w lemma="to" pos="acp" xml:id="A32590-001-a-0220">to</w>
     <w lemma="both" pos="d" xml:id="A32590-001-a-0230">both</w>
     <w lemma="house" pos="n2" xml:id="A32590-001-a-0240">Houses</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-0250">of</w>
     <w lemma="parliament" pos="n1" xml:id="A32590-001-a-0260">Parliament</w>
     <w lemma="at" pos="acp" xml:id="A32590-001-a-0270">at</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-0280">the</w>
     <w lemma="late" pos="j" xml:id="A32590-001-a-0290">late</w>
     <w lemma="prorogation" pos="n1" xml:id="A32590-001-a-0300">Prorogation</w>
     <pc xml:id="A32590-001-a-0310">,</pc>
     <w lemma="to" pos="prt" xml:id="A32590-001-a-0320">to</w>
     <w lemma="let" pos="vvi" xml:id="A32590-001-a-0330">let</w>
     <w lemma="all" pos="d" xml:id="A32590-001-a-0340">all</w>
     <w lemma="our" pos="po" xml:id="A32590-001-a-0350">Our</w>
     <w lemma="subject" pos="n2" xml:id="A32590-001-a-0360">Subjects</w>
     <w lemma="see" pos="vvb" xml:id="A32590-001-a-0370">see</w>
     <w lemma="that" pos="cs" xml:id="A32590-001-a-0380">that</w>
     <w lemma="no" pos="dx" xml:id="A32590-001-a-0390">no</w>
     <w lemma="care" pos="n1" xml:id="A32590-001-a-0400">care</w>
     <w lemma="can" pos="vmb" xml:id="A32590-001-a-0410">can</w>
     <w lemma="be" pos="vvi" xml:id="A32590-001-a-0420">be</w>
     <w lemma="great" pos="jc" xml:id="A32590-001-a-0430">greater</w>
     <w lemma="than" pos="cs" reg="than" xml:id="A32590-001-a-0440">then</w>
     <w lemma="our" pos="po" xml:id="A32590-001-a-0450">Our</w>
     <w lemma="own" pos="d" xml:id="A32590-001-a-0460">own</w>
     <pc xml:id="A32590-001-a-0470">,</pc>
     <w lemma="in" pos="acp" xml:id="A32590-001-a-0480">in</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-0490">the</w>
     <w lemma="effectual" pos="j" xml:id="A32590-001-a-0500">effectual</w>
     <w lemma="suppress" pos="vvg" xml:id="A32590-001-a-0510">Suppressing</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-0520">of</w>
     <w lemma="popery" pos="n1" xml:id="A32590-001-a-0530">Popery</w>
     <pc xml:id="A32590-001-a-0540">,</pc>
     <w lemma="we" pos="pns" xml:id="A32590-001-a-0550">We</w>
     <w lemma="be" pos="vvd" xml:id="A32590-001-a-0560">were</w>
     <w lemma="please" pos="vvn" xml:id="A32590-001-a-0570">pleased</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-0580">the</w>
     <w lemma="fourteen" pos="ord" xml:id="A32590-001-a-0590">Fourteenth</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-0600">of</w>
     <w lemma="this" pos="d" xml:id="A32590-001-a-0610">this</w>
     <w lemma="instant" pos="j" xml:id="A32590-001-a-0620">instant</w>
     <hi xml:id="A32590-e70">
      <w lemma="November" pos="nn1" xml:id="A32590-001-a-0630">November</w>
      <pc xml:id="A32590-001-a-0640">,</pc>
     </hi>
     <w lemma="in" pos="acp" xml:id="A32590-001-a-0650">in</w>
     <w lemma="council" pos="n1" xml:id="A32590-001-a-0660">Council</w>
     <pc xml:id="A32590-001-a-0670">,</pc>
     <w lemma="to" pos="prt" xml:id="A32590-001-a-0680">to</w>
     <w lemma="direct" pos="vvi" xml:id="A32590-001-a-0690">Direct</w>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-0700">and</w>
     <w lemma="command" pos="vvi" xml:id="A32590-001-a-0710">Command</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-0720">the</w>
     <w lemma="lord" pos="n1" xml:id="A32590-001-a-0730">Lord</w>
     <w lemma="steward" pos="n1" xml:id="A32590-001-a-0740">Steward</w>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-0750">and</w>
     <w lemma="lord" pos="n1" xml:id="A32590-001-a-0760">Lord</w>
     <w lemma="chamberlain" pos="n1" xml:id="A32590-001-a-0770">Chamberlain</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-0780">of</w>
     <w lemma="our" pos="po" xml:id="A32590-001-a-0790">Our</w>
     <w lemma="household" pos="n1" xml:id="A32590-001-a-0800">Household</w>
     <w lemma="to" pos="prt" xml:id="A32590-001-a-0810">to</w>
     <w lemma="hinder" pos="vvi" xml:id="A32590-001-a-0820">hinder</w>
     <w lemma="all" pos="d" xml:id="A32590-001-a-0830">all</w>
     <w lemma="papist" pos="nn2" xml:id="A32590-001-a-0840">Papists</w>
     <pc xml:id="A32590-001-a-0850">,</pc>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-0860">and</w>
     <w lemma="popish" pos="j" xml:id="A32590-001-a-0870">Popish</w>
     <w lemma="recusant" pos="n2" xml:id="A32590-001-a-0880">Recusants</w>
     <pc xml:id="A32590-001-a-0890">,</pc>
     <w lemma="or" pos="cc" xml:id="A32590-001-a-0900">or</w>
     <w lemma="repute" pos="vvn" xml:id="A32590-001-a-0910">reputed</w>
     <w lemma="papist" pos="nn2" xml:id="A32590-001-a-0920">Papists</w>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-0930">and</w>
     <w lemma="popish" pos="j" xml:id="A32590-001-a-0940">Popish</w>
     <w lemma="recusant" pos="n2" xml:id="A32590-001-a-0950">Recusants</w>
     <w lemma="from" pos="acp" xml:id="A32590-001-a-0960">from</w>
     <w lemma="have" pos="vvg" xml:id="A32590-001-a-0970">having</w>
     <w lemma="access" pos="n1" xml:id="A32590-001-a-0980">access</w>
     <w lemma="to" pos="acp" xml:id="A32590-001-a-0990">to</w>
     <w lemma="our" pos="po" xml:id="A32590-001-a-1000">Our</w>
     <w lemma="presence" pos="n1" xml:id="A32590-001-a-1010">Presence</w>
     <pc xml:id="A32590-001-a-1020">,</pc>
     <w lemma="or" pos="cc" xml:id="A32590-001-a-1030">or</w>
     <w lemma="to" pos="acp" xml:id="A32590-001-a-1040">to</w>
     <w lemma="our" pos="po" xml:id="A32590-001-a-1050">Our</w>
     <w lemma="palace" pos="n1" xml:id="A32590-001-a-1060">Palace</w>
     <pc xml:id="A32590-001-a-1070">,</pc>
     <w lemma="or" pos="cc" xml:id="A32590-001-a-1080">or</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-1090">the</w>
     <w lemma="place" pos="n1" xml:id="A32590-001-a-1100">place</w>
     <w lemma="where" pos="crq" xml:id="A32590-001-a-1110">where</w>
     <w lemma="our" pos="po" xml:id="A32590-001-a-1120">Our</w>
     <w lemma="court" pos="n1" xml:id="A32590-001-a-1130">Court</w>
     <w lemma="shall" pos="vmb" xml:id="A32590-001-a-1140">shall</w>
     <w lemma="be" pos="vvi" xml:id="A32590-001-a-1150">be</w>
     <pc xml:id="A32590-001-a-1160">,</pc>
     <w lemma="from" pos="acp" xml:id="A32590-001-a-1170">from</w>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-1180">and</w>
     <w lemma="after" pos="acp" xml:id="A32590-001-a-1190">after</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-1200">the</w>
     <w lemma="eighteen" pos="ord" xml:id="A32590-001-a-1210">Eighteenth</w>
     <w lemma="day" pos="n1" xml:id="A32590-001-a-1220">day</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-1230">of</w>
     <w lemma="this" pos="d" xml:id="A32590-001-a-1240">this</w>
     <w lemma="instant" pos="j" xml:id="A32590-001-a-1250">instant</w>
     <hi xml:id="A32590-e80">
      <w lemma="November" pos="nn1" xml:id="A32590-001-a-1260">November</w>
      <pc xml:id="A32590-001-a-1270">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-1280">and</w>
     <w lemma="do" pos="vvd" xml:id="A32590-001-a-1290">did</w>
     <w lemma="then" pos="av" xml:id="A32590-001-a-1300">then</w>
     <w lemma="likewise" pos="av" xml:id="A32590-001-a-1310">likewise</w>
     <w lemma="command" pos="vvb" xml:id="A32590-001-a-1320">Command</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-1330">the</w>
     <w lemma="judge" pos="n2" reg="judges" xml:id="A32590-001-a-1340">Iudges</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-1350">of</w>
     <w lemma="our" pos="po" xml:id="A32590-001-a-1360">Our</w>
     <w lemma="court" pos="n2" xml:id="A32590-001-a-1370">Courts</w>
     <w lemma="at" pos="acp" xml:id="A32590-001-a-1380">at</w>
     <hi xml:id="A32590-e90">
      <w lemma="Westminster" pos="nn1" xml:id="A32590-001-a-1390">Westminster</w>
      <pc xml:id="A32590-001-a-1400">,</pc>
     </hi>
     <w lemma="to" pos="prt" xml:id="A32590-001-a-1410">to</w>
     <w lemma="consider" pos="vvi" xml:id="A32590-001-a-1420">consider</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-1430">of</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-1440">the</w>
     <w lemma="most" pos="avs-d" xml:id="A32590-001-a-1450">most</w>
     <w lemma="effectual" pos="j" xml:id="A32590-001-a-1460">effectual</w>
     <w lemma="mean" pos="n2" xml:id="A32590-001-a-1470">means</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-1480">of</w>
     <w lemma="put" pos="vvg" xml:id="A32590-001-a-1490">putting</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-1500">the</w>
     <w lemma="law" pos="n2" xml:id="A32590-001-a-1510">Laws</w>
     <w lemma="in" pos="acp" xml:id="A32590-001-a-1520">in</w>
     <w lemma="execution" pos="n1" xml:id="A32590-001-a-1530">Execution</w>
     <w lemma="for" pos="acp" xml:id="A32590-001-a-1540">for</w>
     <w lemma="prevent" pos="vvg" xml:id="A32590-001-a-1550">preventing</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-1560">the</w>
     <w lemma="growth" pos="n1" xml:id="A32590-001-a-1570">Growth</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-1580">of</w>
     <w lemma="popery" pos="n1" xml:id="A32590-001-a-1590">Popery</w>
     <pc xml:id="A32590-001-a-1600">,</pc>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-1610">and</w>
     <w lemma="speedy" pos="av-j" xml:id="A32590-001-a-1620">speedily</w>
     <w lemma="to" pos="prt" xml:id="A32590-001-a-1630">to</w>
     <w lemma="report" pos="vvi" xml:id="A32590-001-a-1640">Report</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-1650">the</w>
     <w lemma="same" pos="d" xml:id="A32590-001-a-1660">same</w>
     <w lemma="to" pos="acp" xml:id="A32590-001-a-1670">to</w>
     <w lemma="we" pos="pno" reg="Us" xml:id="A32590-001-a-1680">Vs</w>
     <pc xml:id="A32590-001-a-1690">:</pc>
     <w lemma="now" pos="av" xml:id="A32590-001-a-1700">Now</w>
     <w lemma="for" pos="acp" xml:id="A32590-001-a-1710">for</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-1720">the</w>
     <w lemma="more" pos="avc-d" xml:id="A32590-001-a-1730">more</w>
     <w lemma="effectual" pos="j" xml:id="A32590-001-a-1740">effectual</w>
     <w lemma="suppression" pos="n1" xml:id="A32590-001-a-1750">Suppression</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-1760">of</w>
     <w lemma="popery" pos="n1" xml:id="A32590-001-a-1770">Popery</w>
     <w lemma="in" pos="acp" xml:id="A32590-001-a-1780">in</w>
     <w lemma="all" pos="d" xml:id="A32590-001-a-1790">all</w>
     <w lemma="part" pos="n2" xml:id="A32590-001-a-1800">parts</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-1810">of</w>
     <w lemma="our" pos="po" xml:id="A32590-001-a-1820">Our</w>
     <w lemma="kingdom" pos="n1" xml:id="A32590-001-a-1830">Kingdom</w>
     <pc xml:id="A32590-001-a-1840">,</pc>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-1850">and</w>
     <w lemma="preservation" pos="n1" xml:id="A32590-001-a-1860">Preservation</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-1870">of</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-1880">the</w>
     <w lemma="true" pos="j" xml:id="A32590-001-a-1890">true</w>
     <w lemma="religion" pos="n1" xml:id="A32590-001-a-1900">Religion</w>
     <w lemma="establish" pos="vvn" xml:id="A32590-001-a-1910">Established</w>
     <pc xml:id="A32590-001-a-1920">,</pc>
     <w lemma="we" pos="pns" xml:id="A32590-001-a-1930">We</w>
     <w lemma="do" pos="vvb" xml:id="A32590-001-a-1940">do</w>
     <w lemma="hereby" pos="av" xml:id="A32590-001-a-1950">hereby</w>
     <w lemma="declare" pos="vvi" xml:id="A32590-001-a-1960">Declare</w>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-1970">and</w>
     <w lemma="publish" pos="vvb" xml:id="A32590-001-a-1980">Publish</w>
     <w lemma="our" pos="po" xml:id="A32590-001-a-1990">Our</w>
     <w lemma="further" pos="jc" xml:id="A32590-001-a-2000">further</w>
     <w lemma="will" pos="n1" xml:id="A32590-001-a-2010">Will</w>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-2020">and</w>
     <w lemma="pleasure" pos="n1" xml:id="A32590-001-a-2030">Pleasure</w>
     <pc xml:id="A32590-001-a-2040">,</pc>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-2050">and</w>
     <w lemma="also" pos="av" xml:id="A32590-001-a-2060">also</w>
     <w lemma="strict" pos="av-j" xml:id="A32590-001-a-2070">strictly</w>
     <w lemma="charge" pos="vvb" xml:id="A32590-001-a-2080">Charge</w>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-2090">and</w>
     <w lemma="command" pos="vvi" xml:id="A32590-001-a-2100">Command</w>
     <w lemma="all" pos="d" xml:id="A32590-001-a-2110">all</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-2120">the</w>
     <w lemma="judge" pos="n2" reg="judges" xml:id="A32590-001-a-2130">Iudges</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-2140">of</w>
     <w lemma="our" pos="po" xml:id="A32590-001-a-2150">Our</w>
     <w lemma="court" pos="n2" xml:id="A32590-001-a-2160">Courts</w>
     <w lemma="at" pos="acp" xml:id="A32590-001-a-2170">at</w>
     <hi xml:id="A32590-e100">
      <w lemma="Westminster" pos="nn1" xml:id="A32590-001-a-2180">Westminster</w>
      <pc xml:id="A32590-001-a-2190">,</pc>
     </hi>
     <w lemma="justice" pos="n2" reg="justices" xml:id="A32590-001-a-2200">Iustices</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-2210">of</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-2220">the</w>
     <w lemma="peace" pos="n1" xml:id="A32590-001-a-2230">Peace</w>
     <pc xml:id="A32590-001-a-2240">,</pc>
     <w lemma="Mayor" pos="nn1" reg="Mayor" xml:id="A32590-001-a-2250">Mayors</w>
     <pc xml:id="A32590-001-a-2260">,</pc>
     <w lemma="sheriff" pos="n2" xml:id="A32590-001-a-2270">Sheriffs</w>
     <pc xml:id="A32590-001-a-2280">,</pc>
     <w lemma="bailiff" pos="n2" reg="Bailiffs" xml:id="A32590-001-a-2290">Bayliffs</w>
     <pc xml:id="A32590-001-a-2300">,</pc>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-2310">and</w>
     <w lemma="other" pos="d" xml:id="A32590-001-a-2320">other</w>
     <w lemma="our" pos="po" xml:id="A32590-001-a-2330">Our</w>
     <w lemma="officer" pos="n2" xml:id="A32590-001-a-2340">Officers</w>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-2350">and</w>
     <w lemma="minister" pos="n2" xml:id="A32590-001-a-2360">Ministers</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-2370">of</w>
     <w lemma="justice" pos="n1" reg="justice" xml:id="A32590-001-a-2380">Iustice</w>
     <w lemma="whatsoever" pos="crq" xml:id="A32590-001-a-2390">whatsoever</w>
     <pc xml:id="A32590-001-a-2400">,</pc>
     <w lemma="that" pos="cs" xml:id="A32590-001-a-2410">That</w>
     <w lemma="they" pos="pns" xml:id="A32590-001-a-2420">they</w>
     <w lemma="do" pos="vvb" xml:id="A32590-001-a-2430">do</w>
     <w lemma="forthwith" pos="av" xml:id="A32590-001-a-2440">forthwith</w>
     <w lemma="take" pos="vvi" xml:id="A32590-001-a-2450">take</w>
     <w lemma="effectual" pos="j" xml:id="A32590-001-a-2460">effectual</w>
     <w lemma="care" pos="n1" xml:id="A32590-001-a-2470">care</w>
     <w lemma="for" pos="acp" xml:id="A32590-001-a-2480">for</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-2490">the</w>
     <w lemma="prosecution" pos="n1" xml:id="A32590-001-a-2500">Prosecution</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-2510">of</w>
     <w lemma="all" pos="d" xml:id="A32590-001-a-2520">all</w>
     <w lemma="papist" pos="nn2" xml:id="A32590-001-a-2530">Papists</w>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-2540">and</w>
     <w lemma="popish" pos="j" xml:id="A32590-001-a-2550">Popish</w>
     <w lemma="recusant" pos="n2" xml:id="A32590-001-a-2560">Recusants</w>
     <pc xml:id="A32590-001-a-2570">,</pc>
     <w lemma="according" pos="j" xml:id="A32590-001-a-2580">according</w>
     <w lemma="to" pos="acp" xml:id="A32590-001-a-2590">to</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-2600">the</w>
     <w lemma="law" pos="n2" xml:id="A32590-001-a-2610">Laws</w>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-2620">and</w>
     <w lemma="statute" pos="n2" xml:id="A32590-001-a-2630">Statutes</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-2640">of</w>
     <w lemma="this" pos="d" xml:id="A32590-001-a-2650">this</w>
     <w lemma="realm" pos="n1" xml:id="A32590-001-a-2660">Realm</w>
     <pc xml:id="A32590-001-a-2670">;</pc>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-2680">And</w>
     <w lemma="for" pos="acp" xml:id="A32590-001-a-2690">for</w>
     <w lemma="that" pos="d" xml:id="A32590-001-a-2700">that</w>
     <w lemma="purpose" pos="n1" xml:id="A32590-001-a-2710">purpose</w>
     <pc xml:id="A32590-001-a-2720">,</pc>
     <w lemma="that" pos="cs" xml:id="A32590-001-a-2730">That</w>
     <w lemma="they" pos="pns" xml:id="A32590-001-a-2740">they</w>
     <w lemma="give" pos="vvb" xml:id="A32590-001-a-2750">give</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-2760">the</w>
     <w lemma="say" pos="vvn" xml:id="A32590-001-a-2770">said</w>
     <w lemma="law" pos="n2" xml:id="A32590-001-a-2780">Laws</w>
     <w lemma="in" pos="acp" xml:id="A32590-001-a-2790">in</w>
     <w lemma="charge" pos="n1" xml:id="A32590-001-a-2800">Charge</w>
     <w lemma="at" pos="acp" xml:id="A32590-001-a-2810">at</w>
     <w lemma="their" pos="po" xml:id="A32590-001-a-2820">their</w>
     <w lemma="respective" pos="j" xml:id="A32590-001-a-2830">respective</w>
     <w lemma="assize" pos="n2" xml:id="A32590-001-a-2840">Assizes</w>
     <pc xml:id="A32590-001-a-2850">,</pc>
     <w lemma="gaol-deliveries" pos="j" xml:id="A32590-001-a-2860">Gaol-deliveries</w>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-2870">and</w>
     <w lemma="quarter-session" pos="n2" xml:id="A32590-001-a-2880">Quarter-Sessions</w>
     <pc xml:id="A32590-001-a-2890">,</pc>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-2900">and</w>
     <w lemma="then" pos="av" xml:id="A32590-001-a-2910">then</w>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-2920">and</w>
     <w lemma="there" pos="av" xml:id="A32590-001-a-2930">there</w>
     <w lemma="take" pos="vvb" xml:id="A32590-001-a-2940">take</w>
     <w lemma="order" pos="n1" xml:id="A32590-001-a-2950">order</w>
     <w lemma="that" pos="cs" xml:id="A32590-001-a-2960">that</w>
     <w lemma="such" pos="d" xml:id="A32590-001-a-2970">such</w>
     <w lemma="papist" pos="nn2" xml:id="A32590-001-a-2980">Papists</w>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-2990">and</w>
     <w lemma="popish" pos="j" xml:id="A32590-001-a-3000">Popish</w>
     <w lemma="recusant" pos="n2" xml:id="A32590-001-a-3010">Recusants</w>
     <pc xml:id="A32590-001-a-3020">,</pc>
     <w lemma="or" pos="cc" xml:id="A32590-001-a-3030">or</w>
     <w lemma="person" pos="n2" xml:id="A32590-001-a-3040">persons</w>
     <w lemma="suspect" pos="vvn" xml:id="A32590-001-a-3050">suspected</w>
     <w lemma="to" pos="prt" xml:id="A32590-001-a-3060">to</w>
     <w lemma="be" pos="vvi" xml:id="A32590-001-a-3070">be</w>
     <w lemma="so" pos="av" xml:id="A32590-001-a-3080">so</w>
     <pc xml:id="A32590-001-a-3090">,</pc>
     <w lemma="may" pos="vmb" xml:id="A32590-001-a-3100">may</w>
     <w lemma="be" pos="vvi" xml:id="A32590-001-a-3110">be</w>
     <w lemma="speedy" pos="av-j" xml:id="A32590-001-a-3120">speedily</w>
     <w lemma="present" pos="vvn" xml:id="A32590-001-a-3130">Presented</w>
     <pc xml:id="A32590-001-a-3140">,</pc>
     <w lemma="indict" pos="vvn" xml:id="A32590-001-a-3150">Indicted</w>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-3160">and</w>
     <w lemma="convict" pos="vvn" xml:id="A32590-001-a-3170">Convicted</w>
     <w lemma="according" pos="j" xml:id="A32590-001-a-3180">according</w>
     <w lemma="to" pos="acp" xml:id="A32590-001-a-3190">to</w>
     <w lemma="law" pos="n1" xml:id="A32590-001-a-3200">Law</w>
     <pc xml:id="A32590-001-a-3210">,</pc>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-3220">and</w>
     <w lemma="that" pos="cs" xml:id="A32590-001-a-3230">that</w>
     <w lemma="due" pos="j" xml:id="A32590-001-a-3240">due</w>
     <w lemma="process" pos="n1" xml:id="A32590-001-a-3250">Process</w>
     <w lemma="be" pos="vvi" xml:id="A32590-001-a-3260">be</w>
     <w lemma="from" pos="acp" xml:id="A32590-001-a-3270">from</w>
     <w lemma="time" pos="n1" xml:id="A32590-001-a-3280">time</w>
     <w lemma="to" pos="acp" xml:id="A32590-001-a-3290">to</w>
     <w lemma="time" pos="n1" xml:id="A32590-001-a-3300">time</w>
     <w lemma="issue" pos="vvd" xml:id="A32590-001-a-3310">issued</w>
     <w lemma="thereupon" pos="av" xml:id="A32590-001-a-3320">thereupon</w>
     <pc unit="sentence" xml:id="A32590-001-a-3330">.</pc>
    </p>
    <closer xml:id="A32590-e110">
     <dateline xml:id="A32590-e120">
      <w lemma="give" pos="vvn" xml:id="A32590-001-a-3340">Given</w>
      <w lemma="at" pos="acp" xml:id="A32590-001-a-3350">at</w>
      <w lemma="our" pos="po" xml:id="A32590-001-a-3360">Our</w>
      <w lemma="court" pos="n1" xml:id="A32590-001-a-3370">Court</w>
      <w lemma="at" pos="acp" xml:id="A32590-001-a-3380">at</w>
      <hi xml:id="A32590-e130">
       <w lemma="Whitehall" pos="nn1" xml:id="A32590-001-a-3390">Whitehall</w>
      </hi>
      <date xml:id="A32590-e140">
       <w lemma="the" pos="d" xml:id="A32590-001-a-3400">the</w>
       <w lemma="twenty" pos="ord" xml:id="A32590-001-a-3410">Twentieth</w>
       <w lemma="day" pos="n1" xml:id="A32590-001-a-3420">day</w>
       <w lemma="of" pos="acp" xml:id="A32590-001-a-3430">of</w>
       <hi xml:id="A32590-e150">
        <w lemma="November" pos="nn1" xml:id="A32590-001-a-3440">November</w>
       </hi>
       <w lemma="1673." pos="crd" xml:id="A32590-001-a-3450">1673.</w>
       <pc unit="sentence" xml:id="A32590-001-a-3460"/>
       <w lemma="in" pos="acp" xml:id="A32590-001-a-3470">In</w>
       <w lemma="the" pos="d" xml:id="A32590-001-a-3480">the</w>
       <w lemma="twenty" pos="crd" xml:id="A32590-001-a-3490">Twenty</w>
       <w lemma="five" pos="ord" xml:id="A32590-001-a-3500">fifth</w>
       <w lemma="year" pos="n1" xml:id="A32590-001-a-3510">year</w>
       <w lemma="of" pos="acp" xml:id="A32590-001-a-3520">of</w>
       <w lemma="our" pos="po" xml:id="A32590-001-a-3530">Our</w>
       <w lemma="reign" pos="n1" xml:id="A32590-001-a-3540">Reign</w>
       <pc unit="sentence" xml:id="A32590-001-a-3550">.</pc>
      </date>
     </dateline>
    </closer>
    <closer xml:id="A32590-e160">
     <w lemma="God" pos="nn1" xml:id="A32590-001-a-3560">God</w>
     <w lemma="save" pos="vvb" xml:id="A32590-001-a-3570">save</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-3580">the</w>
     <w lemma="King" pos="n1" xml:id="A32590-001-a-3590">King</w>
     <pc unit="sentence" xml:id="A32590-001-a-3591">.</pc>
     <pc unit="sentence" xml:id="A32590-001-a-3600"/>
    </closer>
   </div>
  </body>
  <back xml:id="A32590-e170">
   <div type="colophon" xml:id="A32590-e180">
    <p xml:id="A32590-e190">
     <hi xml:id="A32590-e200">
      <w lemma="LONDON" pos="nn1" xml:id="A32590-001-a-3610">LONDON</w>
      <pc xml:id="A32590-001-a-3620">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A32590-001-a-3630">Printed</w>
     <w lemma="by" pos="acp" xml:id="A32590-001-a-3640">by</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-3650">the</w>
     <w lemma="assign" pos="vvz" xml:id="A32590-001-a-3660">Assigns</w>
     <w lemma="of" pos="acp" xml:id="A32590-001-a-3670">of</w>
     <hi xml:id="A32590-e210">
      <w lemma="John" pos="nn1" xml:id="A32590-001-a-3680">John</w>
      <w lemma="bill" pos="n1" xml:id="A32590-001-a-3690">Bill</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32590-001-a-3700">and</w>
     <hi xml:id="A32590-e220">
      <w lemma="Christopher" pos="nn1" xml:id="A32590-001-a-3710">Christopher</w>
      <w lemma="Barker" pos="nn1" xml:id="A32590-001-a-3720">Barker</w>
      <pc xml:id="A32590-001-a-3730">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A32590-001-a-3740">Printers</w>
     <w lemma="to" pos="acp" xml:id="A32590-001-a-3750">to</w>
     <w lemma="the" pos="d" xml:id="A32590-001-a-3760">the</w>
     <w lemma="king" pos="n2" xml:id="A32590-001-a-3770">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32590-001-a-3780">most</w>
     <w lemma="excellent" pos="j" xml:id="A32590-001-a-3790">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32590-001-a-3800">Majesty</w>
     <pc unit="sentence" xml:id="A32590-001-a-3810">.</pc>
     <w lemma="1673." pos="crd" xml:id="A32590-001-a-3820">1673.</w>
     <pc unit="sentence" xml:id="A32590-001-a-3830"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
