<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32639">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation requiring the members of both Houses of Parliament to give their attendance upon the fifteenth day of February next</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32639 of text R35965 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing C3571). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A32639</idno>
    <idno type="STC">Wing C3571</idno>
    <idno type="STC">ESTC R35965</idno>
    <idno type="EEBO-CITATION">15584048</idno>
    <idno type="OCLC">ocm 15584048</idno>
    <idno type="VID">103935</idno>
    <idno type="PROQUESTGOID">2264219751</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A32639)</note>
    <note>Transcribed from: (Early English Books Online ; image set 103935)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1588:120)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation requiring the members of both Houses of Parliament to give their attendance upon the fifteenth day of February next</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by the assigns of John Bill and Christopher Barker ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1676.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at our court at Whitehall, the twentieth day of December, in the eight and twentieth year of our reign, 1676.</note>
      <note>Reproduction of the original in the Harvard University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King. A proclamation requiring the members of both Houses of Parliament to give their attendance upon the fifteenth day of February next.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1676</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>253</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-07</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-08</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-09</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2008-09</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32639-t">
  <body xml:id="A32639-e0">
   <div type="royal_proclamation" xml:id="A32639-e10">
    <pb facs="tcp:103935:1" xml:id="A32639-001-a"/>
    <head xml:id="A32639-e20">
     <figure xml:id="A32639-e30">
      <p xml:id="A32639-e40">
       <w lemma="c" pos="sy" xml:id="A32639-001-a-0010">C</w>
       <w lemma="r" pos="sy" xml:id="A32639-001-a-0020">R</w>
      </p>
      <p xml:id="A32639-e50">
       <w lemma="diev" pos="ffr" xml:id="A32639-001-a-0030">DIEV</w>
       <w lemma="et" pos="ffr" xml:id="A32639-001-a-0040">ET</w>
       <w lemma="mon" pos="ffr" xml:id="A32639-001-a-0050">MON</w>
       <w lemma="droit" pos="nn1" xml:id="A32639-001-a-0060">DROIT</w>
      </p>
      <p xml:id="A32639-e60">
       <w lemma="honi" pos="ffr" xml:id="A32639-001-a-0070">HONI</w>
       <w lemma="soit" pos="ffr" xml:id="A32639-001-a-0080">SOIT</w>
       <w lemma="qvi" pos="ffr" xml:id="A32639-001-a-0090">QVI</w>
       <w lemma="mal" pos="ffr" xml:id="A32639-001-a-0100">MAL</w>
       <w lemma="y" pos="ffr" xml:id="A32639-001-a-0110">Y</w>
       <w lemma="pense" pos="ffr" xml:id="A32639-001-a-0120">PENSE</w>
      </p>
      <figDesc xml:id="A32639-e70">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <byline xml:id="A32639-e80">
     <w lemma="by" pos="acp" xml:id="A32639-001-a-0130">By</w>
     <w lemma="the" pos="d" xml:id="A32639-001-a-0140">the</w>
     <w lemma="King" pos="n1" xml:id="A32639-001-a-0150">King</w>
     <pc unit="sentence" xml:id="A32639-001-a-0151">.</pc>
     <pc unit="sentence" xml:id="A32639-001-a-0160"/>
    </byline>
    <head xml:id="A32639-e90">
     <w lemma="a" pos="d" xml:id="A32639-001-a-0170">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A32639-001-a-0180">PROCLAMATION</w>
     <w lemma="require" pos="vvg" xml:id="A32639-001-a-0190">Requiring</w>
     <w lemma="the" pos="d" xml:id="A32639-001-a-0200">the</w>
     <w lemma="member" pos="n2" xml:id="A32639-001-a-0210">Members</w>
     <w lemma="of" pos="acp" xml:id="A32639-001-a-0220">of</w>
     <w lemma="both" pos="d" xml:id="A32639-001-a-0230">both</w>
     <w lemma="house" pos="n2" xml:id="A32639-001-a-0240">Houses</w>
     <w lemma="of" pos="acp" xml:id="A32639-001-a-0250">of</w>
     <w lemma="parliament" pos="n1" xml:id="A32639-001-a-0260">Parliament</w>
     <w lemma="to" pos="prt" xml:id="A32639-001-a-0270">to</w>
     <w lemma="give" pos="vvi" xml:id="A32639-001-a-0280">give</w>
     <w lemma="their" pos="po" xml:id="A32639-001-a-0290">their</w>
     <w lemma="attendance" pos="n1" xml:id="A32639-001-a-0300">Attendance</w>
     <w lemma="upon" pos="acp" xml:id="A32639-001-a-0310">upon</w>
     <w lemma="the" pos="d" xml:id="A32639-001-a-0320">the</w>
     <w lemma="fifteen" pos="ord" xml:id="A32639-001-a-0330">Fifteenth</w>
     <w lemma="day" pos="n1" xml:id="A32639-001-a-0340">day</w>
     <w lemma="of" pos="acp" xml:id="A32639-001-a-0350">of</w>
     <hi xml:id="A32639-e100">
      <w lemma="February" pos="nn1" xml:id="A32639-001-a-0360">February</w>
     </hi>
     <w lemma="next" pos="ord" xml:id="A32639-001-a-0370">next</w>
     <pc unit="sentence" xml:id="A32639-001-a-0380">.</pc>
    </head>
    <opener xml:id="A32639-e110">
     <signed xml:id="A32639-e120">
      <hi xml:id="A32639-e130">
       <w lemma="CHARLES" pos="nn1" xml:id="A32639-001-a-0390">CHARLES</w>
      </hi>
      <w lemma="r." pos="ab" xml:id="A32639-001-a-0400">R.</w>
      <pc unit="sentence" xml:id="A32639-001-a-0410"/>
     </signed>
    </opener>
    <p xml:id="A32639-e140">
     <w lemma="the" pos="d" rend="decorinit" xml:id="A32639-001-a-0420">THe</w>
     <w lemma="king" pos="ng1" reg="King's" xml:id="A32639-001-a-0430">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32639-001-a-0440">most</w>
     <w lemma="excellent" pos="j" xml:id="A32639-001-a-0450">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32639-001-a-0460">Majesty</w>
     <w lemma="have" pos="vvg" xml:id="A32639-001-a-0470">having</w>
     <w lemma="be" pos="vvn" xml:id="A32639-001-a-0480">been</w>
     <w lemma="please" pos="vvn" xml:id="A32639-001-a-0490">pleased</w>
     <w lemma="to" pos="prt" xml:id="A32639-001-a-0500">to</w>
     <w lemma="continue" pos="vvi" xml:id="A32639-001-a-0510">continue</w>
     <w lemma="this" pos="d" xml:id="A32639-001-a-0520">this</w>
     <w lemma="present" pos="j" xml:id="A32639-001-a-0530">present</w>
     <w lemma="parliament" pos="n1" xml:id="A32639-001-a-0540">Parliament</w>
     <w lemma="by" pos="acp" xml:id="A32639-001-a-0550">by</w>
     <w lemma="prorogation" pos="n1" xml:id="A32639-001-a-0560">Prorogation</w>
     <w lemma="until" pos="acp" xml:id="A32639-001-a-0570">until</w>
     <w lemma="the" pos="d" xml:id="A32639-001-a-0580">the</w>
     <w lemma="fifteen" pos="ord" xml:id="A32639-001-a-0590">Fifteenth</w>
     <w lemma="day" pos="n1" xml:id="A32639-001-a-0600">day</w>
     <w lemma="of" pos="acp" xml:id="A32639-001-a-0610">of</w>
     <hi xml:id="A32639-e150">
      <w lemma="February" pos="nn1" xml:id="A32639-001-a-0620">February</w>
     </hi>
     <w lemma="now" pos="av" xml:id="A32639-001-a-0630">now</w>
     <w lemma="next" pos="ord" xml:id="A32639-001-a-0640">next</w>
     <w lemma="come" pos="n1-vg" xml:id="A32639-001-a-0650">coming</w>
     <pc xml:id="A32639-001-a-0660">,</pc>
     <w lemma="with" pos="acp" xml:id="A32639-001-a-0670">with</w>
     <w lemma="a" pos="d" xml:id="A32639-001-a-0680">a</w>
     <w lemma="full" pos="j" xml:id="A32639-001-a-0690">full</w>
     <w lemma="purpose" pos="n1" xml:id="A32639-001-a-0700">purpose</w>
     <w lemma="and" pos="cc" xml:id="A32639-001-a-0710">and</w>
     <w lemma="resolution" pos="n1" xml:id="A32639-001-a-0720">resolution</w>
     <w lemma="that" pos="cs" xml:id="A32639-001-a-0730">that</w>
     <w lemma="the" pos="d" xml:id="A32639-001-a-0740">the</w>
     <w lemma="parliament" pos="n1" xml:id="A32639-001-a-0750">Parliament</w>
     <w lemma="shall" pos="vmb" xml:id="A32639-001-a-0760">shall</w>
     <w lemma="be" pos="vvi" xml:id="A32639-001-a-0770">be</w>
     <w lemma="then" pos="av" xml:id="A32639-001-a-0780">then</w>
     <w lemma="hold" pos="vvn" xml:id="A32639-001-a-0790">holden</w>
     <pc xml:id="A32639-001-a-0800">:</pc>
     <w lemma="and" pos="cc" xml:id="A32639-001-a-0810">And</w>
     <w lemma="be" pos="vvg" xml:id="A32639-001-a-0820">being</w>
     <w lemma="desirous" pos="j" xml:id="A32639-001-a-0830">desirous</w>
     <pc join="right" xml:id="A32639-001-a-0840">(</pc>
     <w lemma="for" pos="acp" xml:id="A32639-001-a-0850">for</w>
     <w lemma="divers" pos="j" xml:id="A32639-001-a-0860">divers</w>
     <w lemma="weighty" pos="j" xml:id="A32639-001-a-0870">weighty</w>
     <w lemma="consideration" pos="n2" xml:id="A32639-001-a-0880">considerations</w>
     <pc xml:id="A32639-001-a-0890">)</pc>
     <w lemma="to" pos="prt" xml:id="A32639-001-a-0900">to</w>
     <w lemma="have" pos="vvi" xml:id="A32639-001-a-0910">have</w>
     <w lemma="then" pos="av" xml:id="A32639-001-a-0920">then</w>
     <w lemma="a" pos="d" xml:id="A32639-001-a-0930">a</w>
     <w lemma="full" pos="j" xml:id="A32639-001-a-0940">full</w>
     <w lemma="assembly" pos="n1" xml:id="A32639-001-a-0950">Assembly</w>
     <w lemma="of" pos="acp" xml:id="A32639-001-a-0960">of</w>
     <w lemma="the" pos="d" xml:id="A32639-001-a-0970">the</w>
     <w lemma="member" pos="n2" xml:id="A32639-001-a-0980">Members</w>
     <w lemma="of" pos="acp" xml:id="A32639-001-a-0990">of</w>
     <w lemma="both" pos="d" xml:id="A32639-001-a-1000">both</w>
     <w lemma="house" pos="n2" xml:id="A32639-001-a-1010">Houses</w>
     <w lemma="of" pos="acp" xml:id="A32639-001-a-1020">of</w>
     <w lemma="parliament" pos="n1" xml:id="A32639-001-a-1030">Parliament</w>
     <pc xml:id="A32639-001-a-1040">,</pc>
     <w lemma="have" pos="vvz" xml:id="A32639-001-a-1050">hath</w>
     <pc join="right" xml:id="A32639-001-a-1060">(</pc>
     <w lemma="with" pos="acp" xml:id="A32639-001-a-1070">with</w>
     <w lemma="the" pos="d" xml:id="A32639-001-a-1080">the</w>
     <w lemma="advice" pos="n1" xml:id="A32639-001-a-1090">Advice</w>
     <w lemma="of" pos="acp" xml:id="A32639-001-a-1100">of</w>
     <w lemma="his" pos="po" xml:id="A32639-001-a-1110">His</w>
     <w lemma="privy" pos="j" xml:id="A32639-001-a-1120">Privy</w>
     <w lemma="council" pos="n1" xml:id="A32639-001-a-1130">Council</w>
     <pc xml:id="A32639-001-a-1140">)</pc>
     <w lemma="think" pos="vvd" xml:id="A32639-001-a-1150">thought</w>
     <w lemma="fit" pos="j" xml:id="A32639-001-a-1160">fit</w>
     <w lemma="to" pos="prt" xml:id="A32639-001-a-1170">to</w>
     <w lemma="declare" pos="vvi" xml:id="A32639-001-a-1180">declare</w>
     <w lemma="and" pos="cc" xml:id="A32639-001-a-1190">and</w>
     <w lemma="publish" pos="vvi" xml:id="A32639-001-a-1200">publish</w>
     <pc xml:id="A32639-001-a-1210">,</pc>
     <w lemma="and" pos="cc" xml:id="A32639-001-a-1220">and</w>
     <w lemma="do" pos="vvz" xml:id="A32639-001-a-1230">doth</w>
     <pc xml:id="A32639-001-a-1240">,</pc>
     <w lemma="by" pos="acp" xml:id="A32639-001-a-1250">by</w>
     <w lemma="this" pos="d" xml:id="A32639-001-a-1260">this</w>
     <w lemma="his" pos="po" xml:id="A32639-001-a-1270">His</w>
     <w lemma="royal" pos="j" xml:id="A32639-001-a-1280">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="A32639-001-a-1290">Proclamation</w>
     <pc xml:id="A32639-001-a-1300">,</pc>
     <w lemma="declare" pos="vvb" xml:id="A32639-001-a-1310">declare</w>
     <w lemma="and" pos="cc" xml:id="A32639-001-a-1320">and</w>
     <w lemma="publish" pos="vvi" xml:id="A32639-001-a-1330">publish</w>
     <w lemma="his" pos="po" xml:id="A32639-001-a-1340">his</w>
     <w lemma="say" pos="j-vn" xml:id="A32639-001-a-1350">said</w>
     <w lemma="resolution" pos="n1" xml:id="A32639-001-a-1360">resolution</w>
     <pc unit="sentence" xml:id="A32639-001-a-1370">.</pc>
     <w lemma="and" pos="cc" xml:id="A32639-001-a-1380">And</w>
     <w lemma="also" pos="av" xml:id="A32639-001-a-1390">also</w>
     <w lemma="do" pos="vvz" xml:id="A32639-001-a-1400">doth</w>
     <w lemma="hereby" pos="av" xml:id="A32639-001-a-1410">hereby</w>
     <w lemma="require" pos="vvi" xml:id="A32639-001-a-1420">require</w>
     <w lemma="all" pos="d" xml:id="A32639-001-a-1430">all</w>
     <w lemma="and" pos="cc" xml:id="A32639-001-a-1440">and</w>
     <w lemma="every" pos="d" xml:id="A32639-001-a-1450">every</w>
     <w lemma="the" pos="d" xml:id="A32639-001-a-1460">the</w>
     <w lemma="peer" pos="n2" xml:id="A32639-001-a-1470">Peers</w>
     <w lemma="of" pos="acp" xml:id="A32639-001-a-1480">of</w>
     <w lemma="this" pos="d" xml:id="A32639-001-a-1490">this</w>
     <w lemma="realm" pos="n1" xml:id="A32639-001-a-1500">Realm</w>
     <pc xml:id="A32639-001-a-1510">,</pc>
     <w lemma="and" pos="cc" xml:id="A32639-001-a-1520">and</w>
     <w lemma="all" pos="d" xml:id="A32639-001-a-1530">all</w>
     <w lemma="and" pos="cc" xml:id="A32639-001-a-1540">and</w>
     <w lemma="every" pos="d" xml:id="A32639-001-a-1550">every</w>
     <w lemma="the" pos="d" xml:id="A32639-001-a-1560">the</w>
     <w lemma="knight" pos="n2" xml:id="A32639-001-a-1570">Knights</w>
     <pc xml:id="A32639-001-a-1580">,</pc>
     <w lemma="citizen" pos="n2" xml:id="A32639-001-a-1590">Citizens</w>
     <w lemma="and" pos="cc" xml:id="A32639-001-a-1600">and</w>
     <w lemma="burgess" pos="n2" xml:id="A32639-001-a-1610">Burgesses</w>
     <w lemma="of" pos="acp" xml:id="A32639-001-a-1620">of</w>
     <w lemma="the" pos="d" xml:id="A32639-001-a-1630">the</w>
     <w lemma="house" pos="n1" xml:id="A32639-001-a-1640">House</w>
     <w lemma="of" pos="acp" xml:id="A32639-001-a-1650">of</w>
     <w lemma="commons" pos="n2" xml:id="A32639-001-a-1660">Commons</w>
     <pc xml:id="A32639-001-a-1670">,</pc>
     <w lemma="to" pos="prt" xml:id="A32639-001-a-1680">to</w>
     <w lemma="give" pos="vvi" xml:id="A32639-001-a-1690">give</w>
     <w lemma="their" pos="po" xml:id="A32639-001-a-1700">their</w>
     <w lemma="attendance" pos="n1" xml:id="A32639-001-a-1710">Attendance</w>
     <w lemma="at" pos="acp" xml:id="A32639-001-a-1720">at</w>
     <hi xml:id="A32639-e160">
      <w lemma="Westminster" pos="nn1" xml:id="A32639-001-a-1730">Westminster</w>
     </hi>
     <w lemma="on" pos="acp" xml:id="A32639-001-a-1740">on</w>
     <w lemma="the" pos="d" xml:id="A32639-001-a-1750">the</w>
     <w lemma="say" pos="vvd" xml:id="A32639-001-a-1760">said</w>
     <w lemma="fifteen" pos="ord" xml:id="A32639-001-a-1770">Fifteenth</w>
     <w lemma="day" pos="n1" xml:id="A32639-001-a-1780">day</w>
     <w lemma="of" pos="acp" xml:id="A32639-001-a-1790">of</w>
     <hi xml:id="A32639-e170">
      <w lemma="February" pos="nn1" xml:id="A32639-001-a-1800">February</w>
     </hi>
     <w lemma="next" pos="ord" xml:id="A32639-001-a-1810">next</w>
     <w lemma="precise" pos="av-j" xml:id="A32639-001-a-1820">precisely</w>
     <pc xml:id="A32639-001-a-1830">;</pc>
     <w lemma="and" pos="cc" xml:id="A32639-001-a-1840">And</w>
     <w lemma="his" pos="po" xml:id="A32639-001-a-1850">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32639-001-a-1860">Majesty</w>
     <w lemma="do" pos="vvz" xml:id="A32639-001-a-1870">doth</w>
     <w lemma="expect" pos="vvi" xml:id="A32639-001-a-1880">expect</w>
     <w lemma="a" pos="d" xml:id="A32639-001-a-1890">a</w>
     <w lemma="ready" pos="j" xml:id="A32639-001-a-1900">ready</w>
     <w lemma="conformity" pos="n1" xml:id="A32639-001-a-1910">conformity</w>
     <w lemma="to" pos="acp" xml:id="A32639-001-a-1920">to</w>
     <w lemma="this" pos="d" xml:id="A32639-001-a-1930">this</w>
     <w lemma="his" pos="po" xml:id="A32639-001-a-1940">His</w>
     <w lemma="royal" pos="j" xml:id="A32639-001-a-1950">Royal</w>
     <w lemma="will" pos="n1" xml:id="A32639-001-a-1960">Will</w>
     <w lemma="and" pos="cc" xml:id="A32639-001-a-1970">and</w>
     <w lemma="pleasure" pos="n1" xml:id="A32639-001-a-1980">Pleasure</w>
     <pc unit="sentence" xml:id="A32639-001-a-1990">.</pc>
    </p>
    <closer xml:id="A32639-e180">
     <dateline xml:id="A32639-e190">
      <w lemma="give" pos="vvn" xml:id="A32639-001-a-2000">Given</w>
      <w lemma="at" pos="acp" xml:id="A32639-001-a-2010">at</w>
      <w lemma="our" pos="po" xml:id="A32639-001-a-2020">Our</w>
      <w lemma="court" pos="n1" xml:id="A32639-001-a-2030">Court</w>
      <w lemma="at" pos="acp" xml:id="A32639-001-a-2040">at</w>
      <hi xml:id="A32639-e200">
       <w lemma="Whitehall" pos="nn1" xml:id="A32639-001-a-2050">Whitehall</w>
      </hi>
      <date xml:id="A32639-e210">
       <w lemma="the" pos="d" xml:id="A32639-001-a-2060">the</w>
       <w lemma="twenty" pos="ord" xml:id="A32639-001-a-2070">Twentieth</w>
       <w lemma="day" pos="n1" xml:id="A32639-001-a-2080">day</w>
       <w lemma="of" pos="acp" xml:id="A32639-001-a-2090">of</w>
       <hi xml:id="A32639-e220">
        <w lemma="December" pos="nn1" xml:id="A32639-001-a-2100">December</w>
        <pc xml:id="A32639-001-a-2110">,</pc>
       </hi>
       <w lemma="in" pos="acp" xml:id="A32639-001-a-2120">in</w>
       <w lemma="the" pos="d" xml:id="A32639-001-a-2130">the</w>
       <w lemma="eight" pos="crd" xml:id="A32639-001-a-2140">Eight</w>
       <w lemma="and" pos="cc" xml:id="A32639-001-a-2150">and</w>
       <w lemma="twenty" pos="ord" xml:id="A32639-001-a-2160">twentieth</w>
       <w lemma="year" pos="n1" xml:id="A32639-001-a-2170">year</w>
       <w lemma="of" pos="acp" xml:id="A32639-001-a-2180">of</w>
       <w lemma="our" pos="po" xml:id="A32639-001-a-2190">Our</w>
       <w lemma="reign" pos="n1" xml:id="A32639-001-a-2200">Reign</w>
       <pc xml:id="A32639-001-a-2210">,</pc>
       <w lemma="1676." pos="crd" xml:id="A32639-001-a-2220">1676.</w>
       <pc unit="sentence" xml:id="A32639-001-a-2230"/>
      </date>
     </dateline>
     <lb xml:id="A32639-e230"/>
     <w lemma="God" pos="nn1" xml:id="A32639-001-a-2240">God</w>
     <w lemma="save" pos="vvb" xml:id="A32639-001-a-2250">save</w>
     <w lemma="the" pos="d" xml:id="A32639-001-a-2260">the</w>
     <w lemma="King" pos="n1" xml:id="A32639-001-a-2270">King</w>
     <pc unit="sentence" xml:id="A32639-001-a-2271">.</pc>
     <pc unit="sentence" xml:id="A32639-001-a-2280"/>
    </closer>
   </div>
  </body>
  <back xml:id="A32639-e240">
   <div type="colophon" xml:id="A32639-e250">
    <p xml:id="A32639-e260">
     <hi xml:id="A32639-e270">
      <w lemma="LONDON" pos="nn1" xml:id="A32639-001-a-2290">LONDON</w>
      <pc xml:id="A32639-001-a-2300">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A32639-001-a-2310">Printed</w>
     <w lemma="by" pos="acp" xml:id="A32639-001-a-2320">by</w>
     <w lemma="the" pos="d" xml:id="A32639-001-a-2330">the</w>
     <w lemma="assign" pos="vvz" xml:id="A32639-001-a-2340">Assigns</w>
     <w lemma="of" pos="acp" xml:id="A32639-001-a-2350">of</w>
     <hi xml:id="A32639-e280">
      <w lemma="John" pos="nn1" xml:id="A32639-001-a-2360">John</w>
      <w lemma="bill" pos="n1" xml:id="A32639-001-a-2370">Bill</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32639-001-a-2380">and</w>
     <hi xml:id="A32639-e290">
      <w lemma="Christopher" pos="nn1" xml:id="A32639-001-a-2390">Christopher</w>
      <w lemma="Barker" pos="nn1" xml:id="A32639-001-a-2400">Barker</w>
      <pc xml:id="A32639-001-a-2410">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A32639-001-a-2420">Printers</w>
     <w lemma="to" pos="acp" xml:id="A32639-001-a-2430">to</w>
     <w lemma="the" pos="d" xml:id="A32639-001-a-2440">the</w>
     <w lemma="king" pos="n2" xml:id="A32639-001-a-2450">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32639-001-a-2460">most</w>
     <w lemma="excellent" pos="j" xml:id="A32639-001-a-2470">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32639-001-a-2480">Majesty</w>
     <pc unit="sentence" xml:id="A32639-001-a-2490">.</pc>
     <w lemma="1676." pos="crd" xml:id="A32639-001-a-2500">1676.</w>
     <pc unit="sentence" xml:id="A32639-001-a-2510"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
