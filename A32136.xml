<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32136">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The Kings Majesties speech to the sixe heads, concerning the Queens going into Holland</title>
    <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32136 of text R39155 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing C2817). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A32136</idno>
    <idno type="STC">Wing C2817</idno>
    <idno type="STC">ESTC R39155</idno>
    <idno type="EEBO-CITATION">18240567</idno>
    <idno type="OCLC">ocm 18240567</idno>
    <idno type="VID">107223</idno>
    <idno type="PROQUESTGOID">2248553351</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A32136)</note>
    <note>Transcribed from: (Early English Books Online ; image set 107223)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1629:65)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The Kings Majesties speech to the sixe heads, concerning the Queens going into Holland</title>
      <author>England and Wales. Sovereign (1625-1649 : Charles I)</author>
      <author>Charles I, King of England, 1600-1649.</author>
      <author>Henrietta Maria, Queen, consort of Charles I, King of England, 1609-1669. Queens Majesties speech to a committee of both Houses at Whitehall, touching her going into Holland.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>[s.n.],</publisher>
      <pubPlace>[London] printed :</pubPlace>
      <date>1641.</date>
     </publicationStmt>
     <notesStmt>
      <note>Place of publication suggested by Wing.</note>
      <note>Includes: The Queens Majesties speech to a committee of both Houses at Whitehall, touching her going into Holland.</note>
      <note>Reproduction of original in the Harvard University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Civil War, 1642-1649.</term>
     <term>Great Britain -- Politics and government -- 1642-1649.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A32136</ep:tcp>
    <ep:estc> R39155</ep:estc>
    <ep:stc> (Wing C2817). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>The Kings Majesties speech to the sixe heads, concerning the Queens going into Holland.</ep:title>
    <ep:author>England and Wales. Sovereign</ep:author>
    <ep:publicationYear>1641</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>282</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-03</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-05</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-06</date><label>John Pas</label>
        Sampled and proofread
      </change>
   <change><date>2008-06</date><label>John Pas</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32136-e10">
  <body xml:id="A32136-e20">
   <pb facs="tcp:107223:1" rend="simple:additions" xml:id="A32136-001-a"/>
   <div type="speech" xml:id="A32136-e30">
    <head xml:id="A32136-e40">
     <w lemma="❧" pos="sy" xml:id="A32136-001-a-0010">❧</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-0020">The</w>
     <w lemma="king" pos="ng1" reg="King's" xml:id="A32136-001-a-0030">Kings</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A32136-001-a-0040">Majesties</w>
     <w lemma="speech" pos="n1" xml:id="A32136-001-a-0050">Speech</w>
     <w lemma="to" pos="acp" xml:id="A32136-001-a-0060">to</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-0070">the</w>
     <w lemma="six" pos="crd" reg="six" xml:id="A32136-001-a-0080">sixe</w>
     <w lemma="head" pos="n2" xml:id="A32136-001-a-0090">Heads</w>
     <pc xml:id="A32136-001-a-0100">,</pc>
     <w lemma="concerning" pos="acp" xml:id="A32136-001-a-0110">concerning</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-0120">the</w>
     <w lemma="queen" pos="n2" xml:id="A32136-001-a-0130">Queens</w>
     <w lemma="go" pos="vvg" xml:id="A32136-001-a-0140">going</w>
     <w lemma="into" pos="acp" xml:id="A32136-001-a-0150">into</w>
     <hi xml:id="A32136-e50">
      <w lemma="Holland" pos="nn1" xml:id="A32136-001-a-0160">Holland</w>
      <pc unit="sentence" xml:id="A32136-001-a-0170">.</pc>
     </hi>
    </head>
    <p xml:id="A32136-e60">
     <w lemma="my" pos="po" xml:id="A32136-001-a-0180">MY</w>
     <w lemma="lord" pos="n2" xml:id="A32136-001-a-0190">Lords</w>
     <pc xml:id="A32136-001-a-0200">,</pc>
     <w lemma="and" pos="cc" xml:id="A32136-001-a-0210">and</w>
     <w lemma="gentleman" pos="n2" xml:id="A32136-001-a-0220">Gentlemen</w>
     <pc xml:id="A32136-001-a-0230">;</pc>
     <w lemma="nothing" pos="pix" xml:id="A32136-001-a-0240">nothing</w>
     <w lemma="but" pos="acp" xml:id="A32136-001-a-0250">but</w>
     <w lemma="exstream" pos="j" xml:id="A32136-001-a-0260">exstream</w>
     <w lemma="necessity" pos="n1" xml:id="A32136-001-a-0270">necessity</w>
     <w lemma="shall" pos="vmb" xml:id="A32136-001-a-0280">shall</w>
     <w lemma="make" pos="vvi" xml:id="A32136-001-a-0290">make</w>
     <w lemma="i" pos="pno" xml:id="A32136-001-a-0300">me</w>
     <w lemma="willing" pos="j" xml:id="A32136-001-a-0310">willing</w>
     <w lemma="at" pos="acp" xml:id="A32136-001-a-0320">at</w>
     <w lemma="this" pos="d" xml:id="A32136-001-a-0330">this</w>
     <w lemma="time" pos="n1" xml:id="A32136-001-a-0340">time</w>
     <w lemma="for" pos="acp" xml:id="A32136-001-a-0350">for</w>
     <w lemma="to" pos="prt" xml:id="A32136-001-a-0360">to</w>
     <w lemma="give" pos="vvi" xml:id="A32136-001-a-0370">give</w>
     <w lemma="consent" pos="n1" xml:id="A32136-001-a-0380">consent</w>
     <w lemma="unto" pos="acp" xml:id="A32136-001-a-0390">unto</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-0400">the</w>
     <w lemma="queen" pos="ng1" reg="Queen's" xml:id="A32136-001-a-0410">Queens</w>
     <w lemma="go" pos="vvg" xml:id="A32136-001-a-0420">going</w>
     <w lemma="out" pos="av" xml:id="A32136-001-a-0430">out</w>
     <w lemma="of" pos="acp" xml:id="A32136-001-a-0440">of</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-0450">the</w>
     <w lemma="land" pos="n1" xml:id="A32136-001-a-0460">Land</w>
     <pc xml:id="A32136-001-a-0470">;</pc>
     <w lemma="and" pos="cc" xml:id="A32136-001-a-0480">and</w>
     <w lemma="i" pos="pns" xml:id="A32136-001-a-0490">I</w>
     <w lemma="shall" pos="vmb" xml:id="A32136-001-a-0500">shall</w>
     <w lemma="be" pos="vvi" xml:id="A32136-001-a-0510">be</w>
     <w lemma="very" pos="av" xml:id="A32136-001-a-0520">very</w>
     <w lemma="sorry" pos="j" xml:id="A32136-001-a-0530">sorry</w>
     <w lemma="if" pos="cs" xml:id="A32136-001-a-0540">if</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-0550">the</w>
     <w lemma="case" pos="n1" xml:id="A32136-001-a-0560">case</w>
     <w lemma="stand" pos="vvb" xml:id="A32136-001-a-0570">stand</w>
     <w lemma="so" pos="av" xml:id="A32136-001-a-0580">so</w>
     <pc xml:id="A32136-001-a-0590">,</pc>
     <w lemma="that" pos="cs" xml:id="A32136-001-a-0600">that</w>
     <w lemma="she" pos="pns" xml:id="A32136-001-a-0610">she</w>
     <w lemma="shall" pos="vmd" xml:id="A32136-001-a-0620">should</w>
     <w lemma="be" pos="vvi" xml:id="A32136-001-a-0630">be</w>
     <w lemma="force" pos="vvn" reg="forced" xml:id="A32136-001-a-0640">forc't</w>
     <w lemma="to" pos="prt" xml:id="A32136-001-a-0650">to</w>
     <w lemma="go" pos="vvi" xml:id="A32136-001-a-0660">go</w>
     <w lemma="to" pos="prt" xml:id="A32136-001-a-0670">to</w>
     <w lemma="preserve" pos="vvi" xml:id="A32136-001-a-0680">preserve</w>
     <w lemma="her" pos="po" xml:id="A32136-001-a-0690">her</w>
     <w lemma="health" pos="n1" xml:id="A32136-001-a-0700">health</w>
     <pc xml:id="A32136-001-a-0710">,</pc>
     <w lemma="and" pos="cc" xml:id="A32136-001-a-0720">and</w>
     <w lemma="i" pos="pns" xml:id="A32136-001-a-0730">I</w>
     <w lemma="give" pos="vvb" xml:id="A32136-001-a-0740">give</w>
     <w lemma="unto" pos="acp" xml:id="A32136-001-a-0750">unto</w>
     <w lemma="both" pos="d" xml:id="A32136-001-a-0760">both</w>
     <w lemma="house" pos="n2" xml:id="A32136-001-a-0770">Houses</w>
     <w lemma="many" pos="d" xml:id="A32136-001-a-0780">many</w>
     <w lemma="thanks" pos="n2" xml:id="A32136-001-a-0790">thanks</w>
     <pc xml:id="A32136-001-a-0800">,</pc>
     <w lemma="for" pos="acp" xml:id="A32136-001-a-0810">for</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-0820">the</w>
     <w lemma="care" pos="n1" xml:id="A32136-001-a-0830">care</w>
     <w lemma="they" pos="pns" xml:id="A32136-001-a-0840">they</w>
     <w lemma="have" pos="vvb" xml:id="A32136-001-a-0850">have</w>
     <w lemma="of" pos="acp" xml:id="A32136-001-a-0860">of</w>
     <w lemma="my" pos="po" xml:id="A32136-001-a-0870">my</w>
     <w lemma="wife" pos="ng1" reg="wife's" xml:id="A32136-001-a-0880">Wives</w>
     <w lemma="health" pos="n1" xml:id="A32136-001-a-0890">health</w>
     <w lemma="and" pos="cc" xml:id="A32136-001-a-0900">and</w>
     <w lemma="contentment" pos="n1" xml:id="A32136-001-a-0910">contentment</w>
     <pc xml:id="A32136-001-a-0920">:</pc>
     <w lemma="therefore" pos="av" xml:id="A32136-001-a-0930">therefore</w>
     <w lemma="i" pos="pns" xml:id="A32136-001-a-0940">I</w>
     <w lemma="desire" pos="vvb" xml:id="A32136-001-a-0950">desire</w>
     <w lemma="there" pos="av" xml:id="A32136-001-a-0960">there</w>
     <w lemma="may" pos="vmb" xml:id="A32136-001-a-0970">may</w>
     <w lemma="be" pos="vvi" xml:id="A32136-001-a-0980">be</w>
     <w lemma="a" pos="d" xml:id="A32136-001-a-0990">a</w>
     <w lemma="committee" pos="n1" xml:id="A32136-001-a-1000">Committee</w>
     <w lemma="of" pos="acp" xml:id="A32136-001-a-1010">of</w>
     <w lemma="both" pos="d" xml:id="A32136-001-a-1020">both</w>
     <w lemma="house" pos="n2" xml:id="A32136-001-a-1030">Houses</w>
     <w lemma="here" pos="av" xml:id="A32136-001-a-1040">here</w>
     <w lemma="to" pos="acp" xml:id="A32136-001-a-1050">to</w>
     <w lemma="morrow" pos="n1" xml:id="A32136-001-a-1060">morrow</w>
     <pc xml:id="A32136-001-a-1070">,</pc>
     <w lemma="at" pos="acp" xml:id="A32136-001-a-1080">at</w>
     <w lemma="three" pos="crd" xml:id="A32136-001-a-1090">three</w>
     <w lemma="a" pos="d" xml:id="A32136-001-a-1100">a</w>
     <w lemma="clock" pos="n1" xml:id="A32136-001-a-1110">clock</w>
     <pc xml:id="A32136-001-a-1120">,</pc>
     <w lemma="to" pos="prt" xml:id="A32136-001-a-1130">to</w>
     <w lemma="attend" pos="vvi" xml:id="A32136-001-a-1140">attend</w>
     <w lemma="my" pos="po" xml:id="A32136-001-a-1150">my</w>
     <w lemma="wife" pos="n1" xml:id="A32136-001-a-1160">Wife</w>
     <pc xml:id="A32136-001-a-1170">,</pc>
     <w lemma="with" pos="acp" xml:id="A32136-001-a-1180">with</w>
     <w lemma="these" pos="d" xml:id="A32136-001-a-1190">these</w>
     <w lemma="reason" pos="n2" xml:id="A32136-001-a-1200">Reasons</w>
     <w lemma="which" pos="crq" xml:id="A32136-001-a-1210">which</w>
     <w lemma="have" pos="vvb" xml:id="A32136-001-a-1220">have</w>
     <w lemma="now" pos="av" xml:id="A32136-001-a-1230">now</w>
     <w lemma="be" pos="vvn" xml:id="A32136-001-a-1240">been</w>
     <w lemma="read" pos="vvn" xml:id="A32136-001-a-1250">read</w>
     <w lemma="to" pos="acp" xml:id="A32136-001-a-1260">to</w>
     <w lemma="i" pos="pno" xml:id="A32136-001-a-1270">me</w>
     <pc unit="sentence" xml:id="A32136-001-a-1280">.</pc>
    </p>
   </div>
   <div type="speech" xml:id="A32136-e70">
    <head xml:id="A32136-e80">
     <w lemma="❧" pos="sy" xml:id="A32136-001-a-1290">❧</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-1300">The</w>
     <w lemma="queen" pos="ng1" reg="Queen's" xml:id="A32136-001-a-1310">Queens</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="A32136-001-a-1320">Majesties</w>
     <w lemma="speech" pos="n1" xml:id="A32136-001-a-1330">Speech</w>
     <w lemma="to" pos="acp" xml:id="A32136-001-a-1340">to</w>
     <w lemma="a" pos="d" xml:id="A32136-001-a-1350">a</w>
     <w lemma="committee" pos="n1" xml:id="A32136-001-a-1360">Committee</w>
     <w lemma="of" pos="acp" xml:id="A32136-001-a-1370">of</w>
     <w lemma="both" pos="d" xml:id="A32136-001-a-1380">both</w>
     <w lemma="house" pos="n2" xml:id="A32136-001-a-1390">Houses</w>
     <w lemma="at" pos="acp" xml:id="A32136-001-a-1400">at</w>
     <hi xml:id="A32136-e90">
      <w lemma="Whitehall" pos="nn1" xml:id="A32136-001-a-1410">Whitehall</w>
      <pc xml:id="A32136-001-a-1420">,</pc>
     </hi>
     <w lemma="touch" pos="vvg" xml:id="A32136-001-a-1430">touching</w>
     <w lemma="her" pos="po" xml:id="A32136-001-a-1440">Her</w>
     <w lemma="go" pos="n1-vg" xml:id="A32136-001-a-1450">going</w>
     <w lemma="into" pos="acp" xml:id="A32136-001-a-1460">into</w>
     <hi xml:id="A32136-e100">
      <w lemma="Holland" pos="nn1" xml:id="A32136-001-a-1470">Holland</w>
      <pc unit="sentence" xml:id="A32136-001-a-1480">.</pc>
     </hi>
    </head>
    <p xml:id="A32136-e110">
     <w lemma="my" pos="po" xml:id="A32136-001-a-1490">MY</w>
     <w lemma="lord" pos="n2" xml:id="A32136-001-a-1500">Lords</w>
     <pc xml:id="A32136-001-a-1510">,</pc>
     <w lemma="and" pos="cc" xml:id="A32136-001-a-1520">and</w>
     <w lemma="gentleman" pos="n2" xml:id="A32136-001-a-1530">Gentlemen</w>
     <w lemma="of" pos="acp" xml:id="A32136-001-a-1540">of</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-1550">the</w>
     <w lemma="house" pos="n1" xml:id="A32136-001-a-1560">House</w>
     <w lemma="of" pos="acp" xml:id="A32136-001-a-1570">of</w>
     <w lemma="commons" pos="n2" xml:id="A32136-001-a-1580">Commons</w>
     <pc xml:id="A32136-001-a-1590">,</pc>
     <w lemma="i" pos="pns" xml:id="A32136-001-a-1600">I</w>
     <w lemma="be" pos="vvm" xml:id="A32136-001-a-1610">am</w>
     <w lemma="thankful" pos="j" reg="thankful" xml:id="A32136-001-a-1620">thankefull</w>
     <w lemma="to" pos="acp" xml:id="A32136-001-a-1630">to</w>
     <w lemma="both" pos="d" xml:id="A32136-001-a-1640">both</w>
     <w lemma="house" pos="n2" xml:id="A32136-001-a-1650">Houses</w>
     <w lemma="of" pos="acp" xml:id="A32136-001-a-1660">of</w>
     <w lemma="parliament" pos="n1" xml:id="A32136-001-a-1670">Parliament</w>
     <pc xml:id="A32136-001-a-1680">,</pc>
     <w lemma="for" pos="acp" xml:id="A32136-001-a-1690">for</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-1700">the</w>
     <w lemma="great" pos="j" xml:id="A32136-001-a-1710">great</w>
     <w lemma="care" pos="n1" xml:id="A32136-001-a-1720">care</w>
     <w lemma="they" pos="pns" xml:id="A32136-001-a-1730">they</w>
     <w lemma="have" pos="vvb" xml:id="A32136-001-a-1740">have</w>
     <w lemma="of" pos="acp" xml:id="A32136-001-a-1750">of</w>
     <w lemma="my" pos="po" xml:id="A32136-001-a-1760">my</w>
     <w lemma="health" pos="n1" xml:id="A32136-001-a-1770">health</w>
     <pc xml:id="A32136-001-a-1780">;</pc>
     <w lemma="and" pos="cc" xml:id="A32136-001-a-1790">and</w>
     <w lemma="for" pos="acp" xml:id="A32136-001-a-1800">for</w>
     <w lemma="their" pos="po" xml:id="A32136-001-a-1810">their</w>
     <w lemma="affection" pos="n2" xml:id="A32136-001-a-1820">affections</w>
     <w lemma="to" pos="acp" xml:id="A32136-001-a-1830">to</w>
     <w lemma="i" pos="pno" xml:id="A32136-001-a-1840">me</w>
     <pc xml:id="A32136-001-a-1850">,</pc>
     <w lemma="hope" pos="vvg" xml:id="A32136-001-a-1860">hoping</w>
     <w lemma="that" pos="cs" xml:id="A32136-001-a-1870">that</w>
     <w lemma="i" pos="pns" xml:id="A32136-001-a-1880">I</w>
     <w lemma="shall" pos="vmb" xml:id="A32136-001-a-1890">shall</w>
     <w lemma="see" pos="vvi" xml:id="A32136-001-a-1900">see</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-1910">the</w>
     <w lemma="effect" pos="n1" xml:id="A32136-001-a-1920">effect</w>
     <w lemma="of" pos="acp" xml:id="A32136-001-a-1930">of</w>
     <w lemma="it" pos="pn" xml:id="A32136-001-a-1940">it</w>
     <pc xml:id="A32136-001-a-1950">:</pc>
     <w lemma="true" pos="av-j" reg="truly" xml:id="A32136-001-a-1960">Truely</w>
     <w lemma="nothing" pos="pix" xml:id="A32136-001-a-1970">nothing</w>
     <w lemma="but" pos="acp" xml:id="A32136-001-a-1980">but</w>
     <w lemma="my" pos="po" xml:id="A32136-001-a-1990">my</w>
     <w lemma="life" pos="n1" xml:id="A32136-001-a-2000">life</w>
     <w lemma="can" pos="vmd" xml:id="A32136-001-a-2010">could</w>
     <w lemma="move" pos="vvi" xml:id="A32136-001-a-2020">move</w>
     <w lemma="i" pos="pno" xml:id="A32136-001-a-2030">me</w>
     <w lemma="to" pos="acp" xml:id="A32136-001-a-2040">to</w>
     <w lemma="this" pos="d" xml:id="A32136-001-a-2050">this</w>
     <w lemma="consideration" pos="n1" xml:id="A32136-001-a-2060">consideration</w>
     <pc xml:id="A32136-001-a-2070">,</pc>
     <w lemma="and" pos="cc" xml:id="A32136-001-a-2080">and</w>
     <w lemma="if" pos="cs" xml:id="A32136-001-a-2090">if</w>
     <w lemma="i" pos="pns" xml:id="A32136-001-a-2100">I</w>
     <w lemma="think" pos="vvd" xml:id="A32136-001-a-2110">thought</w>
     <w lemma="i" pos="pns" xml:id="A32136-001-a-2120">I</w>
     <w lemma="can" pos="vmd" xml:id="A32136-001-a-2130">could</w>
     <w lemma="serve" pos="vvi" xml:id="A32136-001-a-2140">serve</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-2150">the</w>
     <w lemma="king" pos="n1" xml:id="A32136-001-a-2160">King</w>
     <pc xml:id="A32136-001-a-2170">,</pc>
     <w lemma="and" pos="cc" xml:id="A32136-001-a-2180">and</w>
     <w lemma="kingdom" pos="n1" reg="kingdom" xml:id="A32136-001-a-2190">Kingdome</w>
     <w lemma="with" pos="acp" xml:id="A32136-001-a-2200">with</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-2210">the</w>
     <w lemma="hazard" pos="n1" xml:id="A32136-001-a-2220">hazard</w>
     <w lemma="of" pos="acp" xml:id="A32136-001-a-2230">of</w>
     <w lemma="my" pos="po" xml:id="A32136-001-a-2240">my</w>
     <w lemma="life" pos="n1" xml:id="A32136-001-a-2250">life</w>
     <pc xml:id="A32136-001-a-2260">,</pc>
     <w lemma="i" pos="pns" xml:id="A32136-001-a-2270">I</w>
     <w lemma="will" pos="vmd" xml:id="A32136-001-a-2280">would</w>
     <w lemma="do" pos="vvi" xml:id="A32136-001-a-2290">do</w>
     <w lemma="it" pos="pn" xml:id="A32136-001-a-2300">it</w>
     <w lemma="willing" pos="av-j" xml:id="A32136-001-a-2310">willingly</w>
     <pc xml:id="A32136-001-a-2320">,</pc>
     <w lemma="and" pos="cc" xml:id="A32136-001-a-2330">and</w>
     <w lemma="i" pos="pns" xml:id="A32136-001-a-2340">I</w>
     <w lemma="hope" pos="vvb" xml:id="A32136-001-a-2350">hope</w>
     <w lemma="you" pos="pn" xml:id="A32136-001-a-2360">you</w>
     <w lemma="do" pos="vvb" xml:id="A32136-001-a-2370">do</w>
     <w lemma="believe" pos="vvi" reg="believe" xml:id="A32136-001-a-2380">beleeve</w>
     <pc xml:id="A32136-001-a-2390">,</pc>
     <w lemma="i" pos="pns" xml:id="A32136-001-a-2400">I</w>
     <w lemma="be" pos="vvm" xml:id="A32136-001-a-2410">am</w>
     <w lemma="so" pos="av" xml:id="A32136-001-a-2420">so</w>
     <w lemma="much" pos="av-d" xml:id="A32136-001-a-2430">much</w>
     <w lemma="interest" pos="vvn" xml:id="A32136-001-a-2440">interested</w>
     <w lemma="in" pos="acp" xml:id="A32136-001-a-2450">in</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-2460">the</w>
     <w lemma="good" pos="j" xml:id="A32136-001-a-2470">good</w>
     <w lemma="and" pos="cc" xml:id="A32136-001-a-2480">and</w>
     <w lemma="welfare" pos="n1" xml:id="A32136-001-a-2490">welfare</w>
     <w lemma="of" pos="acp" xml:id="A32136-001-a-2500">of</w>
     <w lemma="this" pos="d" xml:id="A32136-001-a-2510">this</w>
     <w lemma="kingdom" pos="n1" reg="kingdom" xml:id="A32136-001-a-2520">Kingdome</w>
     <pc xml:id="A32136-001-a-2530">,</pc>
     <w lemma="that" pos="cs" xml:id="A32136-001-a-2540">that</w>
     <w lemma="i" pos="pns" xml:id="A32136-001-a-2550">I</w>
     <w lemma="shall" pos="vmb" xml:id="A32136-001-a-2560">shall</w>
     <w lemma="never" pos="avx" xml:id="A32136-001-a-2570">never</w>
     <w lemma="in" pos="acp" xml:id="A32136-001-a-2580">in</w>
     <w lemma="my" pos="po" xml:id="A32136-001-a-2590">my</w>
     <w lemma="life" pos="n1" xml:id="A32136-001-a-2600">life</w>
     <w lemma="wish" pos="vvi" xml:id="A32136-001-a-2610">wish</w>
     <w lemma="or" pos="cc" xml:id="A32136-001-a-2620">or</w>
     <w lemma="desire" pos="vvi" xml:id="A32136-001-a-2630">desire</w>
     <w lemma="any" pos="d" xml:id="A32136-001-a-2640">any</w>
     <w lemma="thing" pos="n1" xml:id="A32136-001-a-2650">thing</w>
     <w lemma="that" pos="cs" xml:id="A32136-001-a-2660">that</w>
     <w lemma="may" pos="vmb" xml:id="A32136-001-a-2670">may</w>
     <w lemma="prove" pos="vvi" xml:id="A32136-001-a-2680">prove</w>
     <w lemma="to" pos="acp" xml:id="A32136-001-a-2690">to</w>
     <w lemma="the" pos="d" xml:id="A32136-001-a-2700">the</w>
     <w lemma="prejudice" pos="n1" xml:id="A32136-001-a-2710">prejudice</w>
     <w lemma="of" pos="acp" xml:id="A32136-001-a-2720">of</w>
     <w lemma="it" pos="pn" xml:id="A32136-001-a-2730">it</w>
     <pc unit="sentence" xml:id="A32136-001-a-2740">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A32136-e120">
   <div type="colophon" xml:id="A32136-e130">
    <p xml:id="A32136-e140">
     <hi xml:id="A32136-e150">
      <w lemma="July" pos="nn1" xml:id="A32136-001-a-2750">July</w>
     </hi>
     <w lemma="20." pos="crd" xml:id="A32136-001-a-2760">20.</w>
     <pc unit="sentence" xml:id="A32136-001-a-2770"/>
     <w lemma="print" pos="vvn" xml:id="A32136-001-a-2780">Printed</w>
     <w lemma="anno" pos="fla" xml:id="A32136-001-a-2790">Anno</w>
     <w lemma="dom." pos="ab" xml:id="A32136-001-a-2800">Dom.</w>
     <w lemma="1641." pos="crd" xml:id="A32136-001-a-2820">1641.</w>
     <pc unit="sentence" xml:id="A32136-001-a-2830"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
