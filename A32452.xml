<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32452">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation for preventing the importation of foreign corn in time of plenty</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32452 of text R34814 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing C3355). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A32452</idno>
    <idno type="STC">Wing C3355</idno>
    <idno type="STC">ESTC R34814</idno>
    <idno type="EEBO-CITATION">14866748</idno>
    <idno type="OCLC">ocm 14866748</idno>
    <idno type="VID">102744</idno>
    <idno type="PROQUESTGOID">2240929919</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A32452)</note>
    <note>Transcribed from: (Early English Books Online ; image set 102744)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1566:46)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation for preventing the importation of foreign corn in time of plenty</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by the assigns of John Bill and Christopher Barker ...,</publisher>
      <pubPlace>In the Savoy [i.e. London] :</pubPlace>
      <date>1669.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at our court at Whitehall the 29th day of March, in the one and twentieth year of our reign."</note>
      <note>Reproduction of original in the Huntington Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Grain trade -- Law and legislation -- England.</term>
     <term>Proclamations -- Great Britain.</term>
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King. A proclamation for preventing the importation of foreign corn in time of plenty.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1669</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>309</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-03</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-08</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-09</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2008-09</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32452-t">
  <body xml:id="A32452-e0">
   <div type="royal_proclamation" xml:id="A32452-e10">
    <pb facs="tcp:102744:1" xml:id="A32452-001-a"/>
    <head xml:id="A32452-e20">
     <figure xml:id="A32452-e30">
      <p xml:id="A32452-e40">
       <w lemma="diev" pos="ffr" xml:id="A32452-001-a-0010">DIEV</w>
       <w lemma="et" pos="ffr" xml:id="A32452-001-a-0020">ET</w>
       <w lemma="mon" pos="ffr" xml:id="A32452-001-a-0030">MON</w>
       <w lemma="droit" pos="nn1" xml:id="A32452-001-a-0040">DROIT</w>
      </p>
      <p xml:id="A32452-e50">
       <w lemma="honi" pos="ffr" xml:id="A32452-001-a-0050">HONI</w>
       <w lemma="soit" pos="ffr" xml:id="A32452-001-a-0060">SOIT</w>
       <w lemma="qvi" pos="ffr" xml:id="A32452-001-a-0070">QVI</w>
       <w lemma="mal" pos="ffr" xml:id="A32452-001-a-0080">MAL</w>
       <w lemma="y" pos="ffr" xml:id="A32452-001-a-0090">Y</w>
       <w lemma="pense" pos="ffr" xml:id="A32452-001-a-0100">PENSE</w>
      </p>
      <figDesc xml:id="A32452-e60">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <byline xml:id="A32452-e70">
     <w lemma="by" pos="acp" xml:id="A32452-001-a-0110">By</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-0120">the</w>
     <w lemma="King" pos="n1" xml:id="A32452-001-a-0130">King</w>
     <pc unit="sentence" xml:id="A32452-001-a-0131">.</pc>
     <pc unit="sentence" xml:id="A32452-001-a-0140"/>
    </byline>
    <head xml:id="A32452-e80">
     <w lemma="a" pos="d" xml:id="A32452-001-a-0150">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A32452-001-a-0160">PROCLAMATION</w>
     <w lemma="for" pos="acp" xml:id="A32452-001-a-0170">For</w>
     <w lemma="prevent" pos="vvg" xml:id="A32452-001-a-0180">preventing</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-0190">the</w>
     <w lemma="importation" pos="n1" xml:id="A32452-001-a-0200">Importation</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-0210">of</w>
     <w lemma="foreign" pos="j" xml:id="A32452-001-a-0220">Foreign</w>
     <w lemma="corn" pos="n1" xml:id="A32452-001-a-0230">Corn</w>
     <w lemma="in" pos="acp" xml:id="A32452-001-a-0240">in</w>
     <w lemma="time" pos="n1" xml:id="A32452-001-a-0250">time</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-0260">of</w>
     <w lemma="plenty" pos="n1" xml:id="A32452-001-a-0270">Plenty</w>
     <pc unit="sentence" xml:id="A32452-001-a-0280">.</pc>
    </head>
    <opener xml:id="A32452-e90">
     <signed xml:id="A32452-e100">
      <w lemma="CHARLES" pos="nn1" xml:id="A32452-001-a-0290">CHARLES</w>
      <w lemma="r." pos="ab" xml:id="A32452-001-a-0300">R.</w>
      <pc unit="sentence" xml:id="A32452-001-a-0310"/>
     </signed>
    </opener>
    <p xml:id="A32452-e110">
     <w lemma="the" pos="d" rend="decorinit" xml:id="A32452-001-a-0320">THe</w>
     <w lemma="king" pos="ng1" reg="King's" xml:id="A32452-001-a-0330">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32452-001-a-0340">most</w>
     <w lemma="excellent" pos="j" xml:id="A32452-001-a-0350">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32452-001-a-0360">Majesty</w>
     <w lemma="be" pos="vvg" xml:id="A32452-001-a-0370">being</w>
     <w lemma="advertise" pos="vvn" xml:id="A32452-001-a-0380">advertised</w>
     <pc xml:id="A32452-001-a-0390">,</pc>
     <w lemma="that" pos="cs" xml:id="A32452-001-a-0400">That</w>
     <w lemma="great" pos="j" xml:id="A32452-001-a-0410">great</w>
     <w lemma="quantity" pos="n2" xml:id="A32452-001-a-0420">quantities</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-0430">of</w>
     <w lemma="corn" pos="n1" xml:id="A32452-001-a-0440">Corn</w>
     <w lemma="and" pos="cc" xml:id="A32452-001-a-0450">and</w>
     <w lemma="grain" pos="nn1" xml:id="A32452-001-a-0460">Grain</w>
     <w lemma="be" pos="vvb" xml:id="A32452-001-a-0470">are</w>
     <w lemma="import" pos="vvn" xml:id="A32452-001-a-0480">Imported</w>
     <pc xml:id="A32452-001-a-0490">,</pc>
     <w lemma="and" pos="cc" xml:id="A32452-001-a-0500">and</w>
     <w lemma="more" pos="avc-d" xml:id="A32452-001-a-0510">more</w>
     <w lemma="intend" pos="j-vn" xml:id="A32452-001-a-0520">intended</w>
     <w lemma="to" pos="prt" xml:id="A32452-001-a-0530">to</w>
     <w lemma="be" pos="vvi" xml:id="A32452-001-a-0540">be</w>
     <w lemma="import" pos="vvn" xml:id="A32452-001-a-0550">Imported</w>
     <w lemma="into" pos="acp" xml:id="A32452-001-a-0560">into</w>
     <w lemma="this" pos="d" xml:id="A32452-001-a-0570">this</w>
     <w lemma="his" pos="po" xml:id="A32452-001-a-0580">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32452-001-a-0590">Majesties</w>
     <w lemma="kingdom" pos="n1" xml:id="A32452-001-a-0600">Kingdom</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-0610">of</w>
     <hi xml:id="A32452-e120">
      <w lemma="England" pos="nn1" xml:id="A32452-001-a-0620">England</w>
      <pc xml:id="A32452-001-a-0630">;</pc>
     </hi>
     <w lemma="whereby" pos="crq" xml:id="A32452-001-a-0640">whereby</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-0650">the</w>
     <w lemma="liberty" pos="n1" xml:id="A32452-001-a-0660">Liberty</w>
     <w lemma="for" pos="acp" xml:id="A32452-001-a-0670">for</w>
     <w lemma="transportation" pos="nn1" xml:id="A32452-001-a-0680">Transportation</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-0690">of</w>
     <w lemma="corn" pos="n1" xml:id="A32452-001-a-0700">Corn</w>
     <w lemma="will" pos="vmb" xml:id="A32452-001-a-0710">will</w>
     <w lemma="be" pos="vvi" xml:id="A32452-001-a-0720">be</w>
     <w lemma="render" pos="vvn" reg="rendered" xml:id="A32452-001-a-0730">rendred</w>
     <w lemma="useless" pos="j" xml:id="A32452-001-a-0740">useless</w>
     <w lemma="to" pos="acp" xml:id="A32452-001-a-0750">to</w>
     <w lemma="his" pos="po" xml:id="A32452-001-a-0760">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32452-001-a-0770">Majesties</w>
     <w lemma="subject" pos="n2" xml:id="A32452-001-a-0780">Subjects</w>
     <pc xml:id="A32452-001-a-0790">,</pc>
     <w lemma="the" pos="d" xml:id="A32452-001-a-0800">the</w>
     <w lemma="market" pos="n2" xml:id="A32452-001-a-0810">Markets</w>
     <w lemma="clog" pos="vvn" reg="clogged" xml:id="A32452-001-a-0820">clogg'd</w>
     <pc xml:id="A32452-001-a-0830">,</pc>
     <w lemma="and" pos="cc" xml:id="A32452-001-a-0840">and</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-0850">the</w>
     <w lemma="corn" pos="n1" xml:id="A32452-001-a-0860">Corn</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-0870">of</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-0880">the</w>
     <w lemma="growth" pos="n1" xml:id="A32452-001-a-0890">Growth</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-0900">of</w>
     <w lemma="this" pos="d" xml:id="A32452-001-a-0910">this</w>
     <w lemma="kingdom" pos="n1" xml:id="A32452-001-a-0920">Kingdom</w>
     <w lemma="undersell" pos="vvn" xml:id="A32452-001-a-0930">undersold</w>
     <pc xml:id="A32452-001-a-0940">,</pc>
     <w lemma="to" pos="prt" xml:id="A32452-001-a-0950">to</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-0960">the</w>
     <w lemma="great" pos="j" xml:id="A32452-001-a-0970">great</w>
     <w lemma="discouragement" pos="n1" xml:id="A32452-001-a-0980">discouragement</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-0990">of</w>
     <w lemma="tillage" pos="n1" xml:id="A32452-001-a-1000">Tillage</w>
     <w lemma="and" pos="cc" xml:id="A32452-001-a-1010">and</w>
     <w lemma="husbandry" pos="n1" xml:id="A32452-001-a-1020">Husbandry</w>
     <pc xml:id="A32452-001-a-1030">,</pc>
     <w lemma="and" pos="cc" xml:id="A32452-001-a-1040">and</w>
     <w lemma="diminution" pos="n1" xml:id="A32452-001-a-1050">diminution</w>
     <w lemma="and" pos="cc" xml:id="A32452-001-a-1060">and</w>
     <w lemma="abatement" pos="n1" xml:id="A32452-001-a-1070">abatement</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-1080">of</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-1090">the</w>
     <w lemma="rent" pos="n2" xml:id="A32452-001-a-1100">Rents</w>
     <w lemma="and" pos="cc" xml:id="A32452-001-a-1110">and</w>
     <w lemma="revenue" pos="n2" xml:id="A32452-001-a-1120">Revenues</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-1130">of</w>
     <w lemma="this" pos="d" xml:id="A32452-001-a-1140">this</w>
     <w lemma="kingdom" pos="n1" xml:id="A32452-001-a-1150">Kingdom</w>
     <pc unit="sentence" xml:id="A32452-001-a-1160">.</pc>
     <w lemma="therefore" pos="av" xml:id="A32452-001-a-1170">Therefore</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-1180">the</w>
     <w lemma="king" pos="n2" xml:id="A32452-001-a-1190">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32452-001-a-1200">most</w>
     <w lemma="excellent" pos="j" xml:id="A32452-001-a-1210">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32452-001-a-1220">Majesty</w>
     <pc xml:id="A32452-001-a-1230">,</pc>
     <w lemma="with" pos="acp" xml:id="A32452-001-a-1240">with</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-1250">the</w>
     <w lemma="advice" pos="n1" xml:id="A32452-001-a-1260">Advice</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-1270">of</w>
     <w lemma="his" pos="po" xml:id="A32452-001-a-1280">His</w>
     <w lemma="privy" pos="j" xml:id="A32452-001-a-1290">Privy</w>
     <w lemma="council" pos="n1" xml:id="A32452-001-a-1300">Council</w>
     <pc xml:id="A32452-001-a-1310">,</pc>
     <w lemma="for" pos="acp" xml:id="A32452-001-a-1320">for</w>
     <w lemma="remedy" pos="n1" xml:id="A32452-001-a-1330">remedy</w>
     <w lemma="and" pos="cc" xml:id="A32452-001-a-1340">and</w>
     <w lemma="prevention" pos="n1" xml:id="A32452-001-a-1350">prevention</w>
     <w lemma="hereof" pos="av" xml:id="A32452-001-a-1360">hereof</w>
     <pc xml:id="A32452-001-a-1370">,</pc>
     <w lemma="do" pos="vvz" xml:id="A32452-001-a-1380">Doth</w>
     <w lemma="by" pos="acp" xml:id="A32452-001-a-1390">by</w>
     <w lemma="this" pos="d" xml:id="A32452-001-a-1400">this</w>
     <w lemma="his" pos="po" xml:id="A32452-001-a-1410">His</w>
     <w lemma="royal" pos="j" xml:id="A32452-001-a-1420">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="A32452-001-a-1430">Proclamation</w>
     <w lemma="strait" pos="av-j" reg="straight" xml:id="A32452-001-a-1440">straitly</w>
     <w lemma="forbid" pos="vvn" xml:id="A32452-001-a-1450">Forbid</w>
     <w lemma="and" pos="cc" xml:id="A32452-001-a-1460">and</w>
     <w lemma="prohibit" pos="vvi" xml:id="A32452-001-a-1470">Prohibit</w>
     <w lemma="all" pos="d" xml:id="A32452-001-a-1480">all</w>
     <w lemma="person" pos="n2" xml:id="A32452-001-a-1490">persons</w>
     <w lemma="whatsoever" pos="crq" xml:id="A32452-001-a-1500">whatsoever</w>
     <pc xml:id="A32452-001-a-1510">,</pc>
     <w lemma="alien" pos="n2-j" xml:id="A32452-001-a-1520">Aliens</w>
     <w lemma="and" pos="cc" xml:id="A32452-001-a-1530">and</w>
     <w lemma="denizen" pos="n2" xml:id="A32452-001-a-1540">Denizens</w>
     <pc xml:id="A32452-001-a-1550">,</pc>
     <w lemma="and" pos="cc" xml:id="A32452-001-a-1560">and</w>
     <w lemma="all" pos="d" xml:id="A32452-001-a-1570">all</w>
     <w lemma="his" pos="po" xml:id="A32452-001-a-1580">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32452-001-a-1590">Majesties</w>
     <w lemma="subject" pos="n2" xml:id="A32452-001-a-1600">Subjects</w>
     <pc xml:id="A32452-001-a-1610">,</pc>
     <w lemma="from" pos="acp" xml:id="A32452-001-a-1620">from</w>
     <w lemma="and" pos="cc" xml:id="A32452-001-a-1630">and</w>
     <w lemma="after" pos="acp" xml:id="A32452-001-a-1640">after</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-1650">the</w>
     <w lemma="publish" pos="vvg" xml:id="A32452-001-a-1660">Publishing</w>
     <w lemma="this" pos="d" xml:id="A32452-001-a-1670">this</w>
     <w lemma="his" pos="po" xml:id="A32452-001-a-1680">His</w>
     <w lemma="proclamation" pos="n1" xml:id="A32452-001-a-1690">Proclamation</w>
     <pc xml:id="A32452-001-a-1700">,</pc>
     <w lemma="to" pos="acp" xml:id="A32452-001-a-1710">to</w>
     <w lemma="import" pos="n1" xml:id="A32452-001-a-1720">Import</w>
     <w lemma="or" pos="cc" xml:id="A32452-001-a-1730">or</w>
     <w lemma="cause" pos="n1" xml:id="A32452-001-a-1740">cause</w>
     <w lemma="to" pos="prt" xml:id="A32452-001-a-1750">to</w>
     <w lemma="be" pos="vvi" xml:id="A32452-001-a-1760">be</w>
     <w lemma="import" pos="vvn" xml:id="A32452-001-a-1770">Imported</w>
     <w lemma="into" pos="acp" xml:id="A32452-001-a-1780">into</w>
     <w lemma="any" pos="d" xml:id="A32452-001-a-1790">any</w>
     <w lemma="part" pos="n1" xml:id="A32452-001-a-1800">part</w>
     <w lemma="or" pos="cc" xml:id="A32452-001-a-1810">or</w>
     <w lemma="place" pos="n1" xml:id="A32452-001-a-1820">place</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-1830">of</w>
     <w lemma="this" pos="d" xml:id="A32452-001-a-1840">this</w>
     <w lemma="kingdom" pos="n1" xml:id="A32452-001-a-1850">Kingdom</w>
     <pc xml:id="A32452-001-a-1860">,</pc>
     <w lemma="any" pos="d" xml:id="A32452-001-a-1870">any</w>
     <w lemma="foreign" pos="j" xml:id="A32452-001-a-1880">Foreign</w>
     <w lemma="corn" pos="n1" xml:id="A32452-001-a-1890">Corn</w>
     <w lemma="or" pos="cc" xml:id="A32452-001-a-1900">or</w>
     <w lemma="grain" pos="n1" xml:id="A32452-001-a-1910">Grain</w>
     <pc xml:id="A32452-001-a-1920">,</pc>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-1930">of</w>
     <w lemma="what" pos="crq" xml:id="A32452-001-a-1940">what</w>
     <w lemma="nature" pos="n1" xml:id="A32452-001-a-1950">nature</w>
     <w lemma="or" pos="cc" xml:id="A32452-001-a-1960">or</w>
     <w lemma="kind" pos="n1" xml:id="A32452-001-a-1970">kind</w>
     <w lemma="soever" pos="av" xml:id="A32452-001-a-1980">soever</w>
     <pc xml:id="A32452-001-a-1990">,</pc>
     <w lemma="or" pos="cc" xml:id="A32452-001-a-2000">or</w>
     <w lemma="to" pos="acp" xml:id="A32452-001-a-2010">to</w>
     <w lemma="utter" pos="j" reg="Utter" xml:id="A32452-001-a-2020">Vtter</w>
     <pc xml:id="A32452-001-a-2030">,</pc>
     <w lemma="barter" pos="nn1" xml:id="A32452-001-a-2040">Barter</w>
     <pc xml:id="A32452-001-a-2050">,</pc>
     <w lemma="or" pos="cc" xml:id="A32452-001-a-2060">or</w>
     <w lemma="sell" pos="vvb" xml:id="A32452-001-a-2070">Sell</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-2080">the</w>
     <w lemma="same" pos="d" xml:id="A32452-001-a-2090">same</w>
     <pc xml:id="A32452-001-a-2100">,</pc>
     <w lemma="until" pos="acp" xml:id="A32452-001-a-2110">until</w>
     <w lemma="his" pos="po" xml:id="A32452-001-a-2120">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32452-001-a-2130">Majesties</w>
     <w lemma="pleasure" pos="n1" xml:id="A32452-001-a-2140">Pleasure</w>
     <w lemma="shall" pos="vmb" xml:id="A32452-001-a-2150">shall</w>
     <w lemma="be" pos="vvi" xml:id="A32452-001-a-2160">be</w>
     <w lemma="declare" pos="vvn" xml:id="A32452-001-a-2170">declared</w>
     <pc xml:id="A32452-001-a-2180">,</pc>
     <w lemma="or" pos="cc" xml:id="A32452-001-a-2190">or</w>
     <w lemma="other" pos="d" xml:id="A32452-001-a-2200">other</w>
     <w lemma="order" pos="n1" xml:id="A32452-001-a-2210">Order</w>
     <w lemma="take" pos="vvn" xml:id="A32452-001-a-2220">taken</w>
     <pc xml:id="A32452-001-a-2230">,</pc>
     <w lemma="upon" pos="acp" xml:id="A32452-001-a-2240">upon</w>
     <w lemma="pain" pos="n1" xml:id="A32452-001-a-2250">pain</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-2260">of</w>
     <w lemma="forfeit" pos="vvg" xml:id="A32452-001-a-2270">forfeiting</w>
     <w lemma="all" pos="d" xml:id="A32452-001-a-2280">all</w>
     <w lemma="that" pos="cs" xml:id="A32452-001-a-2290">that</w>
     <w lemma="by" pos="acp" xml:id="A32452-001-a-2300">by</w>
     <w lemma="law" pos="n1" xml:id="A32452-001-a-2310">Law</w>
     <w lemma="be" pos="vvz" xml:id="A32452-001-a-2320">is</w>
     <w lemma="forfeitable" pos="j" xml:id="A32452-001-a-2330">Forfeitable</w>
     <pc xml:id="A32452-001-a-2340">,</pc>
     <w lemma="and" pos="cc" xml:id="A32452-001-a-2350">and</w>
     <w lemma="such" pos="d" xml:id="A32452-001-a-2360">such</w>
     <w lemma="other" pos="d" xml:id="A32452-001-a-2370">other</w>
     <w lemma="punishment" pos="n1" xml:id="A32452-001-a-2380">Punishment</w>
     <w lemma="as" pos="acp" xml:id="A32452-001-a-2390">as</w>
     <w lemma="by" pos="acp" xml:id="A32452-001-a-2400">by</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-2410">the</w>
     <w lemma="prerogative" pos="n1" xml:id="A32452-001-a-2420">Prerogative</w>
     <w lemma="royal" pos="j" xml:id="A32452-001-a-2430">Royal</w>
     <w lemma="may" pos="vmb" xml:id="A32452-001-a-2440">may</w>
     <w lemma="be" pos="vvi" xml:id="A32452-001-a-2450">be</w>
     <w lemma="inflict" pos="vvn" xml:id="A32452-001-a-2460">Inflicted</w>
     <w lemma="upon" pos="acp" xml:id="A32452-001-a-2470">upon</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-2480">the</w>
     <w lemma="contemner" pos="n2" xml:id="A32452-001-a-2490">Contemners</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-2500">of</w>
     <w lemma="his" pos="po" xml:id="A32452-001-a-2510">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A32452-001-a-2520">Majesties</w>
     <w lemma="royal" pos="j" xml:id="A32452-001-a-2530">Royal</w>
     <w lemma="authority" pos="n1" xml:id="A32452-001-a-2540">Authority</w>
     <pc unit="sentence" xml:id="A32452-001-a-2550">.</pc>
    </p>
    <closer xml:id="A32452-e130">
     <dateline xml:id="A32452-e140">
      <w lemma="give" pos="vvn" xml:id="A32452-001-a-2560">Given</w>
      <w lemma="at" pos="acp" xml:id="A32452-001-a-2570">at</w>
      <w lemma="our" pos="po" xml:id="A32452-001-a-2580">Our</w>
      <w lemma="court" pos="n1" xml:id="A32452-001-a-2590">Court</w>
      <w lemma="at" pos="acp" xml:id="A32452-001-a-2600">at</w>
      <hi xml:id="A32452-e150">
       <w lemma="Whitehall" pos="nn1" xml:id="A32452-001-a-2610">Whitehall</w>
      </hi>
      <date xml:id="A32452-e160">
       <w lemma="the" pos="d" xml:id="A32452-001-a-2620">the</w>
       <w lemma="29" orig="29ᵗʰ" pos="ord" xml:id="A32452-001-a-2630">29th</w>
       <w lemma="day" pos="n1" xml:id="A32452-001-a-2650">day</w>
       <w lemma="of" pos="acp" xml:id="A32452-001-a-2660">of</w>
       <hi xml:id="A32452-e180">
        <w lemma="march" pos="n1" xml:id="A32452-001-a-2670">March</w>
        <pc xml:id="A32452-001-a-2680">,</pc>
       </hi>
       <w lemma="in" pos="acp" xml:id="A32452-001-a-2690">in</w>
       <w lemma="the" pos="d" xml:id="A32452-001-a-2700">the</w>
       <w lemma="one" pos="crd" xml:id="A32452-001-a-2710">One</w>
       <w lemma="and" pos="cc" xml:id="A32452-001-a-2720">and</w>
       <w lemma="twenty" pos="ord" xml:id="A32452-001-a-2730">twentieth</w>
       <w lemma="year" pos="n1" xml:id="A32452-001-a-2740">year</w>
       <w lemma="of" pos="acp" xml:id="A32452-001-a-2750">of</w>
       <w lemma="our" pos="po" xml:id="A32452-001-a-2760">Our</w>
       <w lemma="reign" pos="n1" xml:id="A32452-001-a-2770">Reign</w>
       <pc unit="sentence" xml:id="A32452-001-a-2780">.</pc>
      </date>
     </dateline>
     <lb xml:id="A32452-e190"/>
     <w lemma="God" pos="nn1" xml:id="A32452-001-a-2790">God</w>
     <w lemma="save" pos="vvb" xml:id="A32452-001-a-2800">save</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-2810">the</w>
     <w lemma="King" pos="n1" xml:id="A32452-001-a-2820">King</w>
     <pc unit="sentence" xml:id="A32452-001-a-2821">.</pc>
     <pc unit="sentence" xml:id="A32452-001-a-2830"/>
    </closer>
   </div>
  </body>
  <back xml:id="A32452-e200">
   <div type="colophon" xml:id="A32452-e210">
    <p xml:id="A32452-e220">
     <w lemma="in" pos="acp" xml:id="A32452-001-a-2840">In</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-2850">the</w>
     <hi xml:id="A32452-e230">
      <w lemma="Savoy" pos="nn1" xml:id="A32452-001-a-2860">SAVOY</w>
      <pc xml:id="A32452-001-a-2870">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A32452-001-a-2880">Printed</w>
     <w lemma="by" pos="acp" xml:id="A32452-001-a-2890">by</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-2900">the</w>
     <w lemma="assign" pos="vvz" xml:id="A32452-001-a-2910">Assigns</w>
     <w lemma="of" pos="acp" xml:id="A32452-001-a-2920">of</w>
     <hi xml:id="A32452-e240">
      <w lemma="John" pos="nn1" xml:id="A32452-001-a-2930">John</w>
      <w lemma="bill" pos="n1" xml:id="A32452-001-a-2940">Bill</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32452-001-a-2950">and</w>
     <hi xml:id="A32452-e250">
      <w lemma="Christopher" pos="nn1" xml:id="A32452-001-a-2960">Christopher</w>
      <w lemma="Barker" pos="nn1" xml:id="A32452-001-a-2970">Barker</w>
      <pc xml:id="A32452-001-a-2980">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A32452-001-a-2990">Printers</w>
     <w lemma="to" pos="acp" xml:id="A32452-001-a-3000">to</w>
     <w lemma="the" pos="d" xml:id="A32452-001-a-3010">the</w>
     <w lemma="king" pos="n2" xml:id="A32452-001-a-3020">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32452-001-a-3030">most</w>
     <w lemma="excellent" pos="j" xml:id="A32452-001-a-3040">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32452-001-a-3050">Majesty</w>
     <pc xml:id="A32452-001-a-3060">,</pc>
     <w lemma="1669." pos="crd" xml:id="A32452-001-a-3070">1669.</w>
     <pc unit="sentence" xml:id="A32452-001-a-3080"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
