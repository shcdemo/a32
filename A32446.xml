<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A32446">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation for further proroguing the Parliament</title>
    <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A32446 of text R36170 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing C3347). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A32446</idno>
    <idno type="STC">Wing C3347</idno>
    <idno type="STC">ESTC R36170</idno>
    <idno type="EEBO-CITATION">15613762</idno>
    <idno type="OCLC">ocm 15613762</idno>
    <idno type="VID">104141</idno>
    <idno type="PROQUESTGOID">2240893207</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A32446)</note>
    <note>Transcribed from: (Early English Books Online ; image set 104141)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1588:62)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation for further proroguing the Parliament</title>
      <author>England and Wales. Sovereign (1660-1685 : Charles II)</author>
      <author>Charles II, King of England, 1630-1685.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by John Bill, Christopher Barker, Thomas Newcomb and Henry Hills ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1678.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at our court at Whitehall the 25th day of September 1678, in the thirtieth year of our reign."</note>
      <note>Reproduction of original in the Huntington Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King. A proclamation for further proroguing the Parliament.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1678</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>184</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-03</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-08</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-09</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2008-09</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2009-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A32446-t">
  <body xml:id="A32446-e0">
   <div type="royal_proclamation" xml:id="A32446-e10">
    <pb facs="tcp:104141:1" rend="simple:additions" xml:id="A32446-001-a"/>
    <head xml:id="A32446-e20">
     <figure xml:id="A32446-e30">
      <p xml:id="A32446-e40">
       <w lemma="C2R" orig="C²R" pos="sy" xml:id="A32446-001-a-0010">C2R</w>
      </p>
      <p xml:id="A32446-e60">
       <w lemma="diev" pos="ffr" xml:id="A32446-001-a-0040">DIEV</w>
       <w lemma="et" pos="ffr" xml:id="A32446-001-a-0050">ET</w>
       <w lemma="mon" pos="ffr" xml:id="A32446-001-a-0060">MON</w>
       <w lemma="droit" pos="nn1" xml:id="A32446-001-a-0070">DROIT</w>
      </p>
      <p xml:id="A32446-e70">
       <w lemma="honi" pos="ffr" xml:id="A32446-001-a-0080">HONI</w>
       <w lemma="soit" pos="ffr" xml:id="A32446-001-a-0090">SOIT</w>
       <w lemma="qvi" pos="ffr" xml:id="A32446-001-a-0100">QVI</w>
       <w lemma="mal" pos="ffr" xml:id="A32446-001-a-0110">MAL</w>
       <w lemma="y" pos="ffr" xml:id="A32446-001-a-0120">Y</w>
       <w lemma="pense" pos="ffr" xml:id="A32446-001-a-0130">PENSE</w>
      </p>
      <figDesc xml:id="A32446-e80">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <byline xml:id="A32446-e90">
     <w lemma="by" pos="acp" xml:id="A32446-001-a-0140">By</w>
     <w lemma="the" pos="d" xml:id="A32446-001-a-0150">the</w>
     <w lemma="King" pos="n1" xml:id="A32446-001-a-0160">King</w>
     <pc unit="sentence" xml:id="A32446-001-a-0161">.</pc>
     <pc unit="sentence" xml:id="A32446-001-a-0170"/>
    </byline>
    <head xml:id="A32446-e100">
     <w lemma="a" pos="d" xml:id="A32446-001-a-0180">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A32446-001-a-0190">PROCLAMATION</w>
     <w lemma="for" pos="acp" xml:id="A32446-001-a-0200">For</w>
     <w lemma="further" pos="jc" xml:id="A32446-001-a-0210">further</w>
     <w lemma="prorogue" pos="vvg" xml:id="A32446-001-a-0220">Proroguing</w>
     <w lemma="the" pos="d" xml:id="A32446-001-a-0230">the</w>
     <w lemma="parliament" pos="n1" xml:id="A32446-001-a-0240">Parliament</w>
     <pc unit="sentence" xml:id="A32446-001-a-0250">.</pc>
    </head>
    <opener xml:id="A32446-e110">
     <signed xml:id="A32446-e120">
      <w lemma="CHARLES" pos="nn1" xml:id="A32446-001-a-0260">CHARLES</w>
      <w lemma="r." pos="ab" xml:id="A32446-001-a-0270">R.</w>
      <pc unit="sentence" xml:id="A32446-001-a-0280"/>
     </signed>
    </opener>
    <p xml:id="A32446-e130">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A32446-001-a-0290">WHereas</w>
     <w lemma="the" pos="d" xml:id="A32446-001-a-0300">the</w>
     <w lemma="parliament" pos="n1" xml:id="A32446-001-a-0310">Parliament</w>
     <w lemma="be" pos="vvd" xml:id="A32446-001-a-0320">was</w>
     <w lemma="prorogue" pos="vvn" xml:id="A32446-001-a-0330">Prorogued</w>
     <w lemma="unto" pos="acp" xml:id="A32446-001-a-0340">unto</w>
     <w lemma="the" pos="d" xml:id="A32446-001-a-0350">the</w>
     <w lemma="first" pos="ord" xml:id="A32446-001-a-0360">First</w>
     <w lemma="day" pos="n1" xml:id="A32446-001-a-0370">day</w>
     <w lemma="of" pos="acp" xml:id="A32446-001-a-0380">of</w>
     <hi xml:id="A32446-e140">
      <w lemma="October" pos="nn1" xml:id="A32446-001-a-0390">October</w>
     </hi>
     <w lemma="next" pos="ord" xml:id="A32446-001-a-0400">next</w>
     <w lemma="come" pos="vvg" xml:id="A32446-001-a-0410">coming</w>
     <pc xml:id="A32446-001-a-0420">,</pc>
     <w lemma="the" pos="d" xml:id="A32446-001-a-0430">The</w>
     <w lemma="king" pos="n2" xml:id="A32446-001-a-0440">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32446-001-a-0450">most</w>
     <w lemma="excellent" pos="j" xml:id="A32446-001-a-0460">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32446-001-a-0470">Majesty</w>
     <pc join="right" xml:id="A32446-001-a-0480">(</pc>
     <w lemma="by" pos="acp" xml:id="A32446-001-a-0490">by</w>
     <w lemma="the" pos="d" xml:id="A32446-001-a-0500">the</w>
     <w lemma="advice" pos="n1" xml:id="A32446-001-a-0510">Advice</w>
     <w lemma="of" pos="acp" xml:id="A32446-001-a-0520">of</w>
     <w lemma="his" pos="po" xml:id="A32446-001-a-0530">His</w>
     <w lemma="privy" pos="j" xml:id="A32446-001-a-0540">Privy</w>
     <w lemma="council" pos="n1" xml:id="A32446-001-a-0550">Council</w>
     <pc xml:id="A32446-001-a-0560">)</pc>
     <w lemma="for" pos="acp" xml:id="A32446-001-a-0570">for</w>
     <w lemma="divers" pos="j" xml:id="A32446-001-a-0580">divers</w>
     <w lemma="weighty" pos="j" xml:id="A32446-001-a-0590">weighty</w>
     <w lemma="reason" pos="n2" xml:id="A32446-001-a-0600">Reasons</w>
     <pc xml:id="A32446-001-a-0610">,</pc>
     <w lemma="do" pos="vvz" xml:id="A32446-001-a-0620">doth</w>
     <w lemma="by" pos="acp" xml:id="A32446-001-a-0630">by</w>
     <w lemma="this" pos="d" xml:id="A32446-001-a-0640">this</w>
     <w lemma="his" pos="po" xml:id="A32446-001-a-0650">His</w>
     <w lemma="royal" pos="j" xml:id="A32446-001-a-0660">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="A32446-001-a-0670">Proclamation</w>
     <w lemma="further" pos="avc-j" xml:id="A32446-001-a-0680">further</w>
     <w lemma="prorogue" pos="vvi" xml:id="A32446-001-a-0690">Prorogue</w>
     <w lemma="the" pos="d" xml:id="A32446-001-a-0700">the</w>
     <w lemma="same" pos="d" xml:id="A32446-001-a-0710">same</w>
     <w lemma="parliament" pos="n1" xml:id="A32446-001-a-0720">Parliament</w>
     <pc xml:id="A32446-001-a-0730">,</pc>
     <w lemma="and" pos="cc" xml:id="A32446-001-a-0740">and</w>
     <w lemma="the" pos="d" xml:id="A32446-001-a-0750">the</w>
     <w lemma="same" pos="d" xml:id="A32446-001-a-0760">same</w>
     <w lemma="be" pos="vvz" xml:id="A32446-001-a-0770">is</w>
     <w lemma="hereby" pos="av" xml:id="A32446-001-a-0780">hereby</w>
     <w lemma="prorogue" pos="vvn" xml:id="A32446-001-a-0790">Prorogued</w>
     <w lemma="unto" pos="acp" xml:id="A32446-001-a-0800">unto</w>
     <w lemma="the" pos="d" xml:id="A32446-001-a-0810">the</w>
     <w lemma="one" pos="crd" xml:id="A32446-001-a-0820">One</w>
     <w lemma="and" pos="cc" xml:id="A32446-001-a-0830">and</w>
     <w lemma="twenty" pos="ord" xml:id="A32446-001-a-0840">twentieth</w>
     <w lemma="day" pos="n1" xml:id="A32446-001-a-0850">day</w>
     <w lemma="of" pos="acp" xml:id="A32446-001-a-0860">of</w>
     <w lemma="the" pos="d" xml:id="A32446-001-a-0870">the</w>
     <w lemma="same" pos="d" xml:id="A32446-001-a-0880">same</w>
     <w lemma="month" pos="n1" reg="Month" xml:id="A32446-001-a-0890">Moneth</w>
     <pc xml:id="A32446-001-a-0900">:</pc>
     <w lemma="and" pos="cc" xml:id="A32446-001-a-0910">And</w>
     <w lemma="his" pos="po" xml:id="A32446-001-a-0920">His</w>
     <w lemma="majesty" pos="n1" xml:id="A32446-001-a-0930">Majesty</w>
     <w lemma="do" pos="vvz" xml:id="A32446-001-a-0940">doth</w>
     <w lemma="hereby" pos="av" xml:id="A32446-001-a-0950">hereby</w>
     <w lemma="command" pos="n1" xml:id="A32446-001-a-0960">Command</w>
     <w lemma="and" pos="cc" xml:id="A32446-001-a-0970">and</w>
     <w lemma="require" pos="vvi" xml:id="A32446-001-a-0980">Require</w>
     <w lemma="the" pos="d" xml:id="A32446-001-a-0990">the</w>
     <w lemma="lord" pos="n2" xml:id="A32446-001-a-1000">Lords</w>
     <w lemma="spiritual" pos="j" xml:id="A32446-001-a-1010">Spiritual</w>
     <w lemma="and" pos="cc" xml:id="A32446-001-a-1020">and</w>
     <w lemma="temporal" pos="j" xml:id="A32446-001-a-1030">Temporal</w>
     <pc xml:id="A32446-001-a-1040">,</pc>
     <w lemma="and" pos="cc" xml:id="A32446-001-a-1050">and</w>
     <w lemma="the" pos="d" xml:id="A32446-001-a-1060">the</w>
     <w lemma="knight" pos="n2" xml:id="A32446-001-a-1070">Knights</w>
     <pc xml:id="A32446-001-a-1080">,</pc>
     <w lemma="citizen" pos="n2" xml:id="A32446-001-a-1090">Citizens</w>
     <w lemma="and" pos="cc" xml:id="A32446-001-a-1100">and</w>
     <w lemma="burgess" pos="n2" xml:id="A32446-001-a-1110">Burgesses</w>
     <pc xml:id="A32446-001-a-1120">,</pc>
     <w lemma="to" pos="prt" xml:id="A32446-001-a-1130">to</w>
     <w lemma="give" pos="vvi" xml:id="A32446-001-a-1140">give</w>
     <w lemma="their" pos="po" xml:id="A32446-001-a-1150">their</w>
     <w lemma="attendance" pos="n1" xml:id="A32446-001-a-1160">attendance</w>
     <w lemma="at" pos="acp" xml:id="A32446-001-a-1170">at</w>
     <hi xml:id="A32446-e150">
      <w lemma="Westminster" pos="nn1" xml:id="A32446-001-a-1180">Westminster</w>
     </hi>
     <w lemma="on" pos="acp" xml:id="A32446-001-a-1190">on</w>
     <w lemma="the" pos="d" xml:id="A32446-001-a-1200">the</w>
     <w lemma="say" pos="vvd" xml:id="A32446-001-a-1210">said</w>
     <w lemma="one" pos="crd" xml:id="A32446-001-a-1220">One</w>
     <w lemma="and" pos="cc" xml:id="A32446-001-a-1230">and</w>
     <w lemma="twenty" pos="ord" xml:id="A32446-001-a-1240">twentieth</w>
     <w lemma="day" pos="n1" xml:id="A32446-001-a-1250">day</w>
     <w lemma="of" pos="acp" xml:id="A32446-001-a-1260">of</w>
     <hi xml:id="A32446-e160">
      <w lemma="October" pos="nn1" xml:id="A32446-001-a-1270">October</w>
      <pc unit="sentence" xml:id="A32446-001-a-1280">.</pc>
     </hi>
    </p>
    <closer xml:id="A32446-e170">
     <dateline xml:id="A32446-e180">
      <w lemma="give" pos="vvn" xml:id="A32446-001-a-1290">Given</w>
      <w lemma="at" pos="acp" xml:id="A32446-001-a-1300">at</w>
      <w lemma="our" pos="po" xml:id="A32446-001-a-1310">Our</w>
      <w lemma="court" pos="n1" xml:id="A32446-001-a-1320">Court</w>
      <w lemma="at" pos="acp" xml:id="A32446-001-a-1330">at</w>
      <hi xml:id="A32446-e190">
       <w lemma="Whitehall" pos="nn1" xml:id="A32446-001-a-1340">Whitehall</w>
      </hi>
      <date xml:id="A32446-e200">
       <w lemma="the" pos="d" xml:id="A32446-001-a-1350">the</w>
       <w lemma="25" orig="25ᵗʰ" pos="ord" xml:id="A32446-001-a-1360">25th</w>
       <w lemma="day" pos="n1" xml:id="A32446-001-a-1380">day</w>
       <w lemma="of" pos="acp" xml:id="A32446-001-a-1390">of</w>
       <hi xml:id="A32446-e220">
        <w lemma="September" pos="nn1" xml:id="A32446-001-a-1400">September</w>
       </hi>
       <w lemma="1678." pos="crd" xml:id="A32446-001-a-1410">1678.</w>
       <pc unit="sentence" xml:id="A32446-001-a-1420"/>
       <w lemma="in" pos="acp" xml:id="A32446-001-a-1430">In</w>
       <w lemma="the" pos="d" xml:id="A32446-001-a-1440">the</w>
       <w lemma="thirty" pos="ord" xml:id="A32446-001-a-1450">Thirtieth</w>
       <w lemma="year" pos="n1" xml:id="A32446-001-a-1460">year</w>
       <w lemma="of" pos="acp" xml:id="A32446-001-a-1470">of</w>
       <w lemma="our" pos="po" xml:id="A32446-001-a-1480">Our</w>
       <w lemma="reign" pos="n1" xml:id="A32446-001-a-1490">Reign</w>
       <pc unit="sentence" xml:id="A32446-001-a-1500">.</pc>
      </date>
     </dateline>
     <lb xml:id="A32446-e230"/>
     <w lemma="God" pos="nn1" xml:id="A32446-001-a-1510">God</w>
     <w lemma="save" pos="vvb" xml:id="A32446-001-a-1520">save</w>
     <w lemma="the" pos="d" xml:id="A32446-001-a-1530">the</w>
     <w lemma="King" pos="n1" xml:id="A32446-001-a-1540">King</w>
     <pc unit="sentence" xml:id="A32446-001-a-1541">.</pc>
     <pc unit="sentence" xml:id="A32446-001-a-1550"/>
    </closer>
   </div>
  </body>
  <back xml:id="A32446-e240">
   <div type="colophon" xml:id="A32446-e250">
    <p xml:id="A32446-e260">
     <hi xml:id="A32446-e270">
      <w lemma="LONDON" pos="nn1" xml:id="A32446-001-a-1560">LONDON</w>
      <pc xml:id="A32446-001-a-1570">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A32446-001-a-1580">Printed</w>
     <w lemma="by" pos="acp" xml:id="A32446-001-a-1590">by</w>
     <hi xml:id="A32446-e280">
      <w lemma="John" pos="nn1" xml:id="A32446-001-a-1600">John</w>
      <w lemma="bill" pos="n1" xml:id="A32446-001-a-1610">Bill</w>
      <pc xml:id="A32446-001-a-1620">,</pc>
      <w lemma="Christopher" pos="nn1" xml:id="A32446-001-a-1630">Christopher</w>
      <w lemma="Barker" pos="nn1" xml:id="A32446-001-a-1640">Barker</w>
      <pc xml:id="A32446-001-a-1650">,</pc>
      <w lemma="Thomas" pos="nn1" xml:id="A32446-001-a-1660">Thomas</w>
      <w lemma="newcomb" pos="nn1" xml:id="A32446-001-a-1670">Newcomb</w>
      <pc xml:id="A32446-001-a-1680">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A32446-001-a-1690">and</w>
     <hi xml:id="A32446-e290">
      <w lemma="Henry" pos="nn1" xml:id="A32446-001-a-1700">Henry</w>
      <w lemma="hill" pos="n2" xml:id="A32446-001-a-1710">Hills</w>
      <pc xml:id="A32446-001-a-1720">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A32446-001-a-1730">Printers</w>
     <w lemma="to" pos="acp" xml:id="A32446-001-a-1740">to</w>
     <w lemma="the" pos="d" xml:id="A32446-001-a-1750">the</w>
     <w lemma="king" pos="n2" xml:id="A32446-001-a-1760">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A32446-001-a-1770">most</w>
     <w lemma="excellent" pos="j" xml:id="A32446-001-a-1780">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A32446-001-a-1790">Majesty</w>
     <pc unit="sentence" xml:id="A32446-001-a-1800">.</pc>
     <w lemma="1678." pos="crd" xml:id="A32446-001-a-1810">1678.</w>
     <pc unit="sentence" xml:id="A32446-001-a-1820"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
